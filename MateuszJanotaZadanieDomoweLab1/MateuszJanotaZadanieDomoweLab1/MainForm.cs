﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MateuszJanotaZadanieDomoweLab1
{
    public partial class MainForm : Form
    {
        int leaderType; // 1 - fghter // 2 - economist // 3 - middle
        int farmHectars;  // obecna ilość hektarów pola
        int gold; // obecna ilość złota
        int allPeople; // obecna ilość wszystkich ludzi
        int peopleWorkers; // obecna ilość wszystkich pracownikó
        int peopleFreeWorkers; // obecna ilość pracowników gotowych do pracy
        int peopleWorkersGold; // obecna ilość pracowników w kopalni złota
        int peopleWorkersIron; // obecna ilość pracowników w kopalni żelaza
        int peopleWorkersFarm; // obecna ilość pracowników na farmach
        int peopleWorkersCoal; // obecna ilość pracowników w kopalni węgla
        int peopleFighters; // obecna ilość wojowników
        int peopleIntelligence; // obecna ilość ludzi wykształconych
        int peopleHunters; // obecna ilość mysliwych
        int iron; // obecna ilość żelaza
        int coal; // obecna ilość węgla
        int foodMeat; // obecna ilość mięsa
        int foodGrain; // obecna ilość zborza

        bool applicationStop; // zmienna, która gdy true, blokuje wykonywanie się funkcji timer_Tick1
                              // używam jej do tego, aby program nie działał, podczas wyświetlania messageboxa
        bool thereWasACivilWar; // zmienna, która gdy false, oznacza że nie było wojny domowej (wojna domowa
                                // może zdarzyć się tylko raz)


        // Wszystkie poniższe zmienne zaczynające się "time..." oznaczają czas w sekundach i rosną o 0.1 
        // co każde 100 milisekund, a ich wartość w konstruktorze ustawiana jest na 0
        double timeFromStart; // Czas nigdy nie resetowany
        double timeWhenNewPeopleBorn; // Zmienna ustawiana na 0, gdy rodzą się nowi ludzie 
        double timeWhenMaterialsComes; // Zmienna ustawiana na 0, gdy bedzie dostawa materiałów z kopalń i farm
        double timeWhenMessageBoxDoNotShow; // Zmienna ustawiana na 0, gdy pojawi się komunikat o baku jedzenia
                                            // (służy do tego, aby komunikat nie wyświetlał się co sekunde, jeżeli
        //katastrofy:                       // cały czas nie ma jedzenia, a np. co 10 sekund)
        double timeWhenSomeoneAttack; // Zmienna ustawiana na 0, gdy ktoś nas zaatakuje, ma to miejsce co ileś sekund
        double timeWhenHuntersDie; // Zmienna ustawiana na 0, gdy umrą myśliwi w lesie zaatakowani przez zwierzęta
        double timeWhenCivilWar; // Gdy zmienna osiągnie 120, rozpoczyna się wojna domowa
        double timeWhenIllnessKills; // Zmienna ustawiana na 0, gdy nadejdzie choroba
        double timeWhenGasExplodes; // Zmienna ustawiana na 0, gdy wybucha gaz w kopalni

        /// <summary>
        /// konstruktor klasy MainForm, ten kod wykonuje się przy starcie programu
        /// </summary>
        public MainForm()
        {
            InitializeComponent();

            //poniżej ustawiam tło wszystkich labeli w oknie na przeźroczysty
            labelCoal.BackColor = System.Drawing.Color.Transparent;
            labelCoalAmount.BackColor = System.Drawing.Color.Transparent;
            labelFighters.BackColor = System.Drawing.Color.Transparent;
            labelFightersAmount.BackColor = System.Drawing.Color.Transparent;
            labelFoodGrain.BackColor = System.Drawing.Color.Transparent;
            labelFoodGrainAmount.BackColor = System.Drawing.Color.Transparent;
            labelFoodGrainWorkers.BackColor = System.Drawing.Color.Transparent;
            labelFoodMeat.BackColor = System.Drawing.Color.Transparent;
            labelFoodMeatAmount.BackColor = System.Drawing.Color.Transparent;
            labelFoodMeatWorkers.BackColor = System.Drawing.Color.Transparent;
            labelFreeWorkersAmount.BackColor = System.Drawing.Color.Transparent;
            labelFreeWorkersInfo.BackColor = System.Drawing.Color.Transparent;
            labelGold.BackColor = System.Drawing.Color.Transparent;
            labelGoldAmount.BackColor = System.Drawing.Color.Transparent;
            labelHectars.BackColor = System.Drawing.Color.Transparent;
            labelHectarsAmount.BackColor = System.Drawing.Color.Transparent;
            labelHunters.BackColor = System.Drawing.Color.Transparent;
            labelHuntersAmount.BackColor = System.Drawing.Color.Transparent;
            labelInfoAmount.BackColor = System.Drawing.Color.Transparent;
            labelInfoMaterial.BackColor = System.Drawing.Color.Transparent;
            labelInfoWorkers.BackColor = System.Drawing.Color.Transparent;
            labelIntelligence.BackColor = System.Drawing.Color.Transparent;
            labelIntelligenceAmount.BackColor = System.Drawing.Color.Transparent;
            labelIron.BackColor = System.Drawing.Color.Transparent;
            labelIronAmount.BackColor = System.Drawing.Color.Transparent;
            labelPopulation.BackColor = System.Drawing.Color.Transparent;
            labelPopulationAmount.BackColor = System.Drawing.Color.Transparent;
            labelWorkers.BackColor = System.Drawing.Color.Transparent;
            labelWorkersAmount.BackColor = System.Drawing.Color.Transparent;

            // kolor tła textboxów ustawiam na brązowy, aby ładnie komponowały się z tłem
            textBoxCoalWorkers.BackColor = Color.SandyBrown;
            textBoxFarmWorkers.BackColor = Color.SandyBrown;
            textBoxGoldWorkers.BackColor = Color.SandyBrown;
            textBoxIronWorkers.BackColor = Color.SandyBrown;

            applicationStop = false; // ustawiam zmienną applicationStop na false, aby funkcja timer_Tick1 normalnie się wykonywała
            thereWasACivilWar = false; // ustawiam zmienną thereWasACivilWar na false, bo jeszcze nie było wojny domowej


            // poniżej ustawiam wartości początkowe zmiennych
            this.farmHectars = 2;
            this.gold = 100;
            this.allPeople = 10;
            this.peopleWorkers = 10;
            this.peopleIntelligence = 0;
            this.peopleHunters = 0;
            this.peopleFighters = 0;
            this.peopleFreeWorkers = 10;
            this.peopleWorkersCoal = 0;
            this.peopleWorkersFarm = 0;
            this.peopleWorkersGold = 0;
            this.peopleWorkersIron = 0;
            this.iron = 100;
            this.coal = 100;
            this.foodMeat = 100;
            this.foodGrain = 100;
            this.timeFromStart = 0;
            this.timeWhenMaterialsComes = 0;
            this.timeWhenNewPeopleBorn = 0;
            this.timeWhenMessageBoxDoNotShow = 0;
            this.timeWhenSomeoneAttack = 0;
            this.timeWhenHuntersDie = 0;
            this.timeWhenCivilWar = 0;
            this.timeWhenIllnessKills = 0;
            this.timeWhenGasExplodes = 0;

            timer.Interval = 100; // ustawiam też interwał timera na 100 milisekund, czyli funkcja
                                  // timer_Tick1 będzie wykonywała się co 100 milisekund
        }

        /// Poniżej settery, służące do zmiany zawartości zmiennych surówców z TradeForma

        /// <summary>
        /// gold setter
        /// </summary>
        /// <param name="gold"></param>
        public void setGold(int gold)
        {
            this.gold = gold;
        }
        /// <summary>
        /// iron setter
        /// </summary>
        /// <param name="iron"></param>
        public void setIron(int iron)
        {
            this.iron = iron;
        }
        /// <summary>
        /// coal setter
        /// </summary>
        /// <param name="coal"></param>
        public void setCoal(int coal)
        {
            this.coal = coal;
        }
        /// <summary>
        /// foodMeat setter
        /// </summary>
        /// <param name="foodMeat"></param>
        public void setFoodMeat(int foodMeat)
        {
            this.foodMeat = foodMeat;
        }
        /// <summary>
        /// foodGrain setter
        /// </summary>
        /// <param name="foodGrain"></param>
        public void setFoodGrain(int foodGrain)
        {
            this.foodGrain = foodGrain;
        }


        /// <summary>
        /// Zmiana wyglądu po naciśnięciu przycisku wyboru wójta
        /// ustawiam wszystkim elementom z pierwszego ekranu
        /// atrybut visibla na false, dzięki temu one po prostu znikają
        /// </summary>
        void chooseLeaderGUIChange()
        {
            buttonChooseEconomist.Visible = false;
            buttonChooseFighter.Visible = false;
            buttonChooseMiddleLeader.Visible = false;
            labelDecriptionMiddleLeader.Visible = false;
            labelDectriptionEconomistLeader.Visible = false;
            labelDescriptionFighter.Visible = false;
            labelChooseLeade.Visible = false;
            timer.Start();
        }


        /// <summary>
        /// Funkcja ta umożliwia aktualizacje danych, tzn we wszystkich labelach
        /// (oraz gdy parametr FullUpdate == true to również w textboxach) jest
        /// aktualizowana treść na podstawie zmiennych znajdujących się w tej klasie
        /// </summary>
        /// <param name="FullUpdate"></param>
        void updateGUI(bool FullUpdate)
        {
            /////////////////////////////////////////////Aktualizacja ilości surowców
            labelGoldAmount.Text = gold.ToString();
            labelCoalAmount.Text = coal.ToString();
            labelIronAmount.Text = iron.ToString();
            labelFoodMeatAmount.Text = foodMeat.ToString();
            labelFoodGrainAmount.Text = foodGrain.ToString();
            labelHectarsAmount.Text = farmHectars.ToString();
            /////////////////////////////////////////////Aktualizacja ludzi pracujących
            labelFoodGrainWorkers.Text = peopleWorkersFarm.ToString();
            labelFoodMeatWorkers.Text = peopleHunters.ToString();
            if(FullUpdate)
            {
                //////////////////////////////////////Aktualizacja textboxów, jeżeli FullUpdate == true
                textBoxCoalWorkers.Text = peopleWorkersCoal.ToString();
                textBoxFarmWorkers.Text = peopleWorkersFarm.ToString();
                textBoxGoldWorkers.Text = peopleWorkersGold.ToString();
                textBoxIronWorkers.Text = peopleWorkersIron.ToString();
            }
            /////////////////////////////////////////////Aktualizacja ilości ludzi
            allPeople = peopleFighters + peopleHunters + peopleIntelligence + peopleWorkers; //liczenie wszystkich ludzi
            labelWorkersAmount.Text = peopleWorkers.ToString();
            labelIntelligenceAmount.Text = peopleIntelligence.ToString();
            labelHuntersAmount.Text = peopleHunters.ToString();
            labelFightersAmount.Text = peopleFighters.ToString();
            labelFreeWorkersAmount.Text = peopleFreeWorkers.ToString();
            labelPopulationAmount.Text = allPeople.ToString();
        }


        /// <summary>
        /// Efektywne rysowanie obiektów typu label/textbox/buton...
        /// zależne od czasu, oraz rysowanie tła pasami
        /// </summary>
        /// <param name="time"></param>
        void drawGui(double time)
        {
            if (time > 0.0) // Za każdym razem gdy minie trochę czasu dorysowuje jakieś obiekty
            {
                labelInfoMaterial.Visible = true;
                labelFoodMeatAmount.Visible = true;
            }
            if (time > 0.2)// Za każdym razem gdy minie trochę czasu dorysowuje jakieś obiekty
            {
                labelHectars.Visible = true;
                labelFoodGrainAmount.Visible = true;
            }
            if (time > 0.4)// Za każdym razem gdy minie trochę czasu dorysowuje jakieś obiekty
            {
                labelPopulation.Visible = true;
                labelCoalAmount.Visible = true;
            }
            if (time > 0.6)// Za każdym razem gdy minie trochę czasu dorysowuje jakieś obiekty
            {
                labelGold.Visible = true;
                labelIronAmount.Visible = true;
            }
            if (time > 0.8)// Za każdym razem gdy minie trochę czasu dorysowuje jakieś obiekty
            {
                labelIron.Visible = true;
                labelGoldAmount.Visible = true;
            }
            if (time > 1.0)// Za każdym razem gdy minie trochę czasu dorysowuje jakieś obiekty
            {
                labelCoal.Visible = true;
                labelPopulationAmount.Visible = true;
            }
            if (time > 1.2)// Za każdym razem gdy minie trochę czasu dorysowuje jakieś obiekty
            {
                labelFoodGrain.Visible = true;
                labelHectarsAmount.Visible = true;
            }
            if (time > 1.4)// Za każdym razem gdy minie trochę czasu dorysowuje jakieś obiekty
            {
                labelFoodMeat.Visible = true;
                labelInfoAmount.Visible = true;
            }
            if (time > 2.0)// Za każdym razem gdy minie trochę czasu dorysowuje jakieś obiekty
            {
                labelInfoWorkers.Visible = true;
                labelFreeWorkersInfo.Visible = true;
                labelFreeWorkersAmount.Visible = true;
            }
            if (time > 2.2)// Za każdym razem gdy minie trochę czasu dorysowuje jakieś obiekty
            {
                textBoxFarmWorkers.Visible = true;
                buttonCoalWorkersConfirm.Visible = true;
            }
            if (time > 2.4)// Za każdym razem gdy minie trochę czasu dorysowuje jakieś obiekty
            {
                labelWorkers.Visible = true;
                labelWorkersAmount.Visible = true;
                labelFighters.Visible = true;
                labelFightersAmount.Visible = true;
                buttonIronWorkersConfirm.Visible = true;
            }
            if (time > 2.6)// Za każdym razem gdy minie trochę czasu dorysowuje jakieś obiekty
            {
                textBoxGoldWorkers.Visible = true;
                buttonGoldWorkersConfirm.Visible = true;
            }
            if (time > 2.8)// Za każdym razem gdy minie trochę czasu dorysowuje jakieś obiekty
            {
                labelIntelligence.Visible = true;
                labelHunters.Visible = true;
                labelIntelligenceAmount.Visible = true;
                labelHuntersAmount.Visible = true;
                textBoxIronWorkers.Visible = true;
            }
            if (time > 3.0)// Za każdym razem gdy minie trochę czasu dorysowuje jakieś obiekty
            {
                textBoxCoalWorkers.Visible = true;
                buttonFarmLandWorkersConfirm.Visible = true;
            }
            if (time > 3.2)// Za każdym razem gdy minie trochę czasu dorysowuje jakieś obiekty
            {
                labelFoodGrainWorkers.Visible = true;
                buttonExit.Visible = true;
            }
            if (time > 3.4)// Za każdym razem gdy minie trochę czasu dorysowuje jakieś obiekty
            {
                labelFoodMeatWorkers.Visible = true;
                buttonAttackNearWillage.Visible = true;
            }
            if (time > 3.6)// Za każdym razem gdy minie trochę czasu dorysowuje jakieś obiekty
            {
                buttonTrade.Visible = true;
            }
            if (time > 3.8) // Rysuje tło paskami, żeby był ładny efekt (myślałem, że będzie ładniejszy)
            {
                Image myImage = new Bitmap("wioska1.png"); // każdy następny plik, to zdjęcie, które jest
                this.BackgroundImage = myImage;            // nieco bardziej zapełnione, niż poprzednie
            }                                              // dzięki czemu, tło pojawia się pasami przez
            if (time > 4.0)                                // około półtorej sekundy
            {
                Image myImage = new Bitmap("wioska2.png");
                this.BackgroundImage = myImage; // zczytywanie zmiennej myImage i wrzucanie do tła
            }
            if (time > 4.2)
            {
                Image myImage = new Bitmap("wioska3.png");
                this.BackgroundImage = myImage;
            }
            if (time > 4.4)
            {
                Image myImage = new Bitmap("wioska4.png");
                this.BackgroundImage = myImage;
            }
            if (time > 4.6)
            {
                Image myImage = new Bitmap("wioska5.png");
                this.BackgroundImage = myImage;
            }
            if (time > 4.8)
            {
                Image myImage = new Bitmap("wioska6.png");
                this.BackgroundImage = myImage;
            }
            if (time > 5.0)
            {
                Image myImage = new Bitmap("wioska7.png");
                this.BackgroundImage = myImage;
            }
            if (time > 5.2)
            {
                Image myImage = new Bitmap("wioska8.png");
                this.BackgroundImage = myImage;
            }
        }

        /// <summary>
        /// Funkcja timer_Tick_1 jest najczęściej wykonywującą się funkcją w programie
        /// wykonuje się ona automatycznie co 100 milisekund (interwał ustawiany w konstruktorzze),
        /// jednak tylko wtedy, gdy zmienna applicationStop jest ustawiona na false,
        /// zmienną tą ustawiam na false wtedy, gdy wyświetlam jakieś okno dialogowe, np
        /// TradeForm, bądź też messagebox, po to, aby w tle nie wykonywał się program.
        /// funkcja oblicza, ile surowców zostało wydobytych co 1500 milisekund,
        /// oblicza też, ile ludzi się urodziło (ludzie rodzą się co dwie sekundy)
        /// ilu z tych ludzi zostało wojownikami, ilu myśliwymi, z ilu składa się inteligencja...
        /// (jest to wyliczane losowo, skorzystałem z klasy Random)
        /// sprawdza, ile czasu zostało do katastrofy i jeżeli ten czas nastąpił to realizuje 
        /// tę katastrofę. wykonuje się tylko wtedy, gdy nie ma otwartego żadnego pomniejszego okna
        /// dialogowego, takiego jak TradeForm, czy też MessageBoxy.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_Tick_1(object sender, EventArgs e)
        {
            if(!applicationStop) // cały kod funkcji znajduje się w tym ifie
            {
                timeFromStart += 0.1; // ogólny czas od początku działania programu
                timeWhenNewPeopleBorn += 0.1; // czas, który resetuje, gdy rodzą się ludzie
                timeWhenMaterialsComes += 0.1; // czas, który resetuje, gdy pojawiają się materiały
                timeWhenMessageBoxDoNotShow += 0.1; // czas, który resetuje, gdy pokaże się
                //ostrzegawczy messagebox. (zabezpieczenie, aby nie pokazywał się on co sekunde, gdy np nie ma jedzenia)
                timeWhenSomeoneAttack += 0.1; // po przekroczeniu danej wartości tej zmiennej nastąpi katastrofa atak
                timeWhenHuntersDie += 0.1; // po przekroczeniu danej wartości tej zmiennej nastąpi katastrofa myśliwych 
                timeWhenCivilWar += 0.1; // po przekroczeniu danej wartości tej zmiennej nastąpi katastrofa wojna domowa
                timeWhenIllnessKills += 0.1; // po przekroczeniu danej wartości tej zmiennej nastąpi katastrofa choroba
                timeWhenGasExplodes += 0.1; // po przekroczeniu danej wartości tej zmiennej nastąpi katastrofa wybóch gazu w kopalni

                if(timeWhenSomeoneAttack > 80) // jeżeli zmienna timeWhenSomeoneAttack przekroczy daną wartość, natąpi jedna z katastrof
                {
                    if(leaderType == 1)
                    {
                        //jeżeli leaderType == 1, czyli wybrany jest wójt wjownik, to atak zostanie odparty
                        applicationStop = true; // zmienna, która gdy true, blokuje wykonywanie się obecnej funkcji
                        MessageBox.Show("Zaatakowano cię, na szczęście twój wspaniały wójt wojownik odparł atak bez strat");
                        applicationStop = false;
                    }
                    else
                    {
                        applicationStop = true; // zmienna, która gdy true, blokuje wykonywanie się obecnej funkcji
                        MessageBox.Show("Zaatakowano cię, tracisz żołnieży, myśliwych, surowce i hektary pola");
                        applicationStop = false;
                        coal = coal / 2; // zmniejszam surowce o połowe
                        gold = gold / 2; // zmniejszam surowce o połowe
                        iron = iron / 2; // zmniejszam surowce o połowe
                        foodGrain = foodGrain / 2; // zmniejszam surowce o połowe
                        foodMeat = foodMeat / 2; // zmniejszam surowce o połowe
                        peopleHunters = 0; // zeruje myśliwych
                        peopleFighters = 0; // zeruje wojowników
                        if(farmHectars>2)
                        {
                            farmHectars = farmHectars / 3; // zmniejszam pola o dwie trzecie
                                                          //jeżeli były co najmniej trzy
                        }
                        else
                        {
                            farmHectars = 1; // jeżeli były dwa pola lub jedno, to ustawiam pola na 1
                        }
                        ///jeżeli na farmach pracowało więcej ludzi niż obecnie po utracie pola
                        ///może się na nich zmieścić, to ci ludzie się tracą i pracuje teraz
                        ///na polach maksymalna liczba ludzi, jaka może tam pracować, czyli 10 
                        ///robotników na hektar, jeżeli jednak pracowało tam tyle ludzi,
                        ///że po utracie pola dalej się oni mieszczą na farmach, program
                        ///pozostawi to bez zmian
                        if (peopleWorkersFarm > farmHectars * 10)
                        {
                            peopleWorkersFarm = farmHectars * 10;
                        }
                        updateGUI(true); // w związku z solidną zmianą w ilości ludzi i surowców, aktualizuje GUI
                    }
                    timeWhenSomeoneAttack = 0; //zeruje czas, by za kolejne 80 seknud znowu się wydarzyło
                }

                if (timeWhenHuntersDie > 103)// jeżeli zmienna timeWhenSomeoneAttack przekroczy daną wartość, natąpi jedna z katastrof
                {
                    applicationStop = true; // zmienna, która gdy true, blokuje wykonywanie się obecnej funkcji
                    MessageBox.Show("Twoi myśliwi zostali zaatakowani przez agresywne dziki, połowa nie żyje :(");
                    applicationStop = false;
                    peopleHunters = peopleHunters / 2; // ustawiam myśliwych na połowe tego co było
                    updateGUI(true); // w związku z zmianą w ilości ludzi aktualizuje GUI
                    timeWhenHuntersDie = 0;//zeruje czas, by za kolejne 103 seknudy znowu się wydarzyło
                }

                if(timeWhenCivilWar > 120)// jeżeli zmienna timeWhenCivilWar przekroczy daną wartość, natąpi jedna z katastrof
                {
                    ///Wojna domowa może wydarzyć się tylko raz, kod wykona się, jeżeli jeszcze jej nie było
                    if(!thereWasACivilWar)
                    {
                        if (timeWhenCivilWar > 125) // po pięciu sekundach ustawiam zmienną thereWasACivilWar na true
                        {                           // co oznacza, że była już wojna domowa (wojna domowa odbywa się tylko raz)
                            thereWasACivilWar = true;
                        }
                        if(timeWhenCivilWar < 120.11)
                        {
                            applicationStop = true; // zmienna, która gdy true, blokuje wykonywanie się obecnej funkcji
                            MessageBox.Show("Wojna domowa w twoim państwie, giną ludzie przez następne 5 sekund!");
                            applicationStop = false;
                        }
                        if (timeWhenCivilWar < 125) // wojna trwa pięć sekund
                        {
                            if (peopleIntelligence > 0) 
                            {
                                peopleIntelligence -= 1; // co każde 100 milisekund usuwam po jednym człowieku z grupy społecznej inteligencja 
                            }
                            if (peopleHunters > 0)
                            {
                                peopleHunters -= 1;// co każde 100 milisekund usuwam po jednym człowieku z grupy społecznej myśliwi 
                            }
                            if (peopleWorkersCoal > 0)
                            {
                                peopleWorkersCoal -= 1;// co każde 100 milisekund usuwam po jednym robotniku kopalni węgla
                            }
                            if (peopleWorkersFarm > 0)
                            {
                                peopleWorkersFarm -= 1;// co każde 100 milisekund usuwam po jednym robotniku na farmach
                            }
                            if (peopleWorkersGold > 0)
                            {
                                peopleWorkersGold -= 1;// co każde 100 milisekund usuwam po jednym robotniku kopalni złota
                            }
                            if (peopleWorkersIron > 0)
                            {
                                peopleWorkersIron -= 1;// co każde 100 milisekund usuwam po jednym robotniku kopalni żelaza
                            }
                            if (peopleFighters > 0)
                            {
                                peopleFighters -= 1;// co każde 100 milisekund usuwam po jednym człowieku z grupy społecznej wojownicy
                            }
                            peopleWorkers = peopleWorkersCoal + peopleWorkersFarm // aktualizuje ogólną ilość robotników, po tym jak 
                            + peopleWorkersGold + peopleWorkersIron + peopleFreeWorkers;//wielu poginęło w kopalniach i na farmach
                        }
                        this.updateGUI(true); // pełny update GUI po zmianie ilości ludzi
                    }
                }

                if(timeWhenIllnessKills > 200)// jeżeli zmienna timeWhenIllnessKills przekroczy daną wartość, natąpi jedna z katastrof
                {
                    applicationStop = true; // zmienna, która gdy true, blokuje wykonywanie się obecnej funkcji
                    MessageBox.Show("W twojej wiosce rozprzestrzeniła się zaraza, tracisz masę ludzi....");
                    applicationStop = false;
                    peopleIntelligence -= (int)(0.8*peopleIntelligence); // usuwam 80% ludności
                    peopleHunters -= (int)(0.8 * peopleHunters);// usuwam 80% ludności
                    peopleWorkersCoal -= (int)(0.8 * peopleWorkersCoal);// usuwam 80% ludności
                    peopleWorkersFarm -= (int)(0.8 * peopleWorkersFarm);// usuwam 80% ludności
                    peopleWorkersGold -= (int)(0.8 * peopleWorkersGold);// usuwam 80% ludności
                    peopleWorkersIron -= (int)(0.8 * peopleWorkersIron);// usuwam 80% ludności
                    peopleFighters -= (int)(0.8 * peopleFighters);// usuwam 80% ludności
                    peopleFreeWorkers -= (int)(0.8 * peopleFreeWorkers);// usuwam 80% ludności
                    peopleWorkers = peopleWorkersCoal + peopleWorkersFarm // aktualizuje ogólną ilość robotników, po tym jak 
                    + peopleWorkersGold + peopleWorkersIron + peopleFreeWorkers;//wielu poginęło w kopalniach i na farmach
                    this.updateGUI(true); // pełny update GUI po zmianie ilości ludzi
                    timeWhenIllnessKills = 0;
                }
                Random rand = new Random();
                if (timeWhenGasExplodes > 159) // jeżeli zmienna timeWhenGasExplodes przekroczy daną wartość, natąpi jedna z katastrof
                {
                    int randomMineExplode = rand.Next(1, 7); // losuje kopalnie i ogrom wybuchu
                    if (randomMineExplode == 1) 
                    {
                        applicationStop = true; // zmienna, która gdy true, blokuje wykonywanie się obecnej funkcji
                        MessageBox.Show("Wybuchł gaz w kopalni złota, wszyscy robotnicy nie żyją :(");
                        applicationStop = false;
                        peopleWorkers -= peopleWorkersGold; // zmniejszam ilość pracowników w kopalniach
                        peopleWorkersGold = 0; // zmniejszam ilość pracowników w kopalniach
                    }
                    else if(randomMineExplode == 2)
                    {
                        applicationStop = true; // zmienna, która gdy true, blokuje wykonywanie się obecnej funkcji
                        MessageBox.Show("Wybuchł gaz w kopalni węgla, wszyscy robotnicy nie żyją :(");
                        applicationStop = false;
                        peopleWorkers -= peopleWorkersCoal; // zmniejszam ilość pracowników w kopalniach
                        peopleWorkersCoal = 0; // zmniejszam ilość pracowników w kopalniach
                    }
                    else if (randomMineExplode == 3)
                    {
                        applicationStop = true; // zmienna, która gdy true, blokuje wykonywanie się obecnej funkcji
                        MessageBox.Show("Wybuchł gaz w kopalni żelaza, wszyscy robotnicy nie żyją :(");
                        applicationStop = false;
                        peopleWorkers -= peopleWorkersIron; // zmniejszam ilość pracowników w kopalniach
                        peopleWorkersIron = 0; // zmniejszam ilość pracowników w kopalniach
                    }
                    else if (randomMineExplode == 4)
                    {
                        applicationStop = true; // zmienna, która gdy true, blokuje wykonywanie się obecnej funkcji
                        MessageBox.Show("Wybuchł gaz w kopalni złota, połowa robotników nie żyje :(");
                        applicationStop = false;
                        peopleWorkers -= peopleWorkersGold / 2; // zmniejszam ilość pracowników w kopalniach
                        peopleWorkersGold -= peopleWorkersGold / 2; // zmniejszam ilość pracowników w kopalniach
                    }
                    else if (randomMineExplode == 5)
                    {
                        applicationStop = true; // zmienna, która gdy true, blokuje wykonywanie się obecnej funkcji
                        MessageBox.Show("Wybuchł gaz w kopalni węgla, połowa robotników nie żyje:(");
                        applicationStop = false;
                        peopleWorkers -= peopleWorkersCoal / 2; // zmniejszam ilość pracowników w kopalniach
                        peopleWorkersCoal -= peopleWorkersCoal / 2; // zmniejszam ilość pracowników w kopalniach
                    }
                    else if (randomMineExplode == 6)
                    {
                        applicationStop = true; // zmienna, która gdy true, blokuje wykonywanie się obecnej funkcji
                        MessageBox.Show("Wybuchł gaz w kopalni żelaza, połowa robotników nie żyje :(");
                        applicationStop = false;
                        peopleWorkers -= peopleWorkersIron / 2; // zmniejszam ilość pracowników w kopalniach
                        peopleWorkersIron -= peopleWorkersIron / 2; // zmniejszam ilość pracowników w kopalniach
                    }
                    else if (randomMineExplode == 7) // jeden niegroźny przypadek
                    {
                        applicationStop = true; // zmienna, która gdy true, blokuje wykonywanie się obecnej funkcji
                        MessageBox.Show("Wybuchł gaz w jednej z kopalń, na szczęście wszyscy przeżyli");
                        applicationStop = false;
                    }
                    timeWhenGasExplodes = 0;
                    this.updateGUI(true); // jeżeli wybucha gaz, to giną ludzie w kopalniach, wiec pełny update
                                //gui wraz z textboxami, które informują użytkownika o tym, ile osób pracuje w kopalni
                }

                if (timeFromStart < 6) // rysowanie GUI odbywa się pierwsze 6 sekund, potem nie ma potrzeby
                {                      // wykonywać funkcji
                    this.drawGui(timeFromStart);
                }

                if (timeWhenMaterialsComes > 1.5) // Kod w tym if'ie wykonuje się co półtorej sekundy
                {
                    int random;
                    random = rand.Next(1, 3); // losuje ile ludzie zjedzą w tym momencie
                    this.coal += this.peopleWorkersCoal;//Zwiększam materiały w zależności
                    this.gold += this.peopleWorkersGold;//od tego, ile jest pracowników na
                    this.foodGrain += this.peopleWorkersFarm;//dany materiał
                    this.iron += this.peopleWorkersIron;
                    this.foodMeat += this.peopleHunters;
                    timeWhenMaterialsComes = 0; // resetuje czas, żeby ten blok wykonał się
                                                // za kolejne półtorej sekundy
                    if (random == 1)// to ile ludzie zjedzą w danej chwili, losuje się
                    {
                        foodMeat -= this.peopleFighters / 5; //odejmuje od mięsa i zborza 
                        foodGrain -= this.allPeople / 6; //odpowiednie ilości
                    }
                    else if (random == 2) // to ile ludzie zjedzą w danej chwili, losuje się
                    {
                        foodMeat -= this.allPeople / 10;//odejmuje od mięsa i zborza 
                        foodGrain -= this.allPeople / 10;//odpowiednie ilości
                    }
                    if (foodMeat < 0) // jeżeli braknie mięsa, to umierają ludzie
                    {
                        foodMeat = 0;
                        this.peopleFighters -= this.peopleFighters / 10; // zmniejszam populację o 10%
                        this.peopleFreeWorkers -= this.peopleFreeWorkers / 10; // zmniejszam populację o 10%
                        this.peopleWorkersCoal -= this.peopleWorkersCoal / 10; // zmniejszam populację o 10%
                        this.peopleWorkersFarm -= this.peopleWorkersFarm / 10; // zmniejszam populację o 10%
                        this.peopleWorkersGold -= this.peopleWorkersGold / 10; // zmniejszam populację o 10%
                        this.peopleHunters -= this.peopleHunters / 10; // zmniejszam populację o 10%
                        this.peopleWorkersIron -= this.peopleWorkersIron / 10; // zmniejszam populację o 10%
                        this.peopleIntelligence -= this.peopleIntelligence / 10; // zmniejszam populację o 10%
                        //W następnej linijce sprawdzam, ile jest ludzi pracujących w sumie
                        //i aktualizuje zmienną
                        this.peopleWorkers = this.peopleWorkersCoal + this.peopleWorkersFarm
                            + this.peopleWorkersGold + this.peopleWorkersIron + this.peopleFreeWorkers;
                        this.allPeople = this.peopleWorkers + this.peopleHunters + this.peopleFighters + this.peopleIntelligence;
                        if (timeWhenMessageBoxDoNotShow > 10) // Za kolejne półtorej sekundy, jeżeli 
                        {//gracz nic nie zrobi, to znowu braknie jedzenia, aby komunikat nie wyświetlał
                         //się co półtorej sekundy, stworzyłem zmienną timeWhenMessageBoxDoNotShow,
                         //którą resetuje, gdy komunikat się pojawił, gdy zmienna osiągnie wartość 10,
                         //komunikat znów będzie mógł się pojawić
                            applicationStop = true; // zmienna, która gdy true, blokuje wykonywanie się obecnej funkcji
                            MessageBox.Show("Brakuje mięsa, twoi ludzie umierają z głodu"); //messagebox zatrzymuje kod,
                            applicationStop = false;// ale za kolejne 100 milisekund (interval timer) znowu ta funkcja się wykona
                            timeWhenMessageBoxDoNotShow = 0;// dlatego używam zmiennej applicationStop
                            this.updateGUI(true); // pełna aktualizacja GUI, bo jeżeli zginęli ludzie w kopalniach,
                                                  //to należy zaktualizować textboxy, które mówią o ilości ludzi w kopalniach
                        }

                    }
                    if (foodGrain < 0) // analogiczna sytuacja, co z brakiem mięsa:
                    {
                        foodGrain = 0;
                        this.peopleFighters -= this.peopleFighters / 10;
                        this.peopleFreeWorkers -= this.peopleFreeWorkers / 10;
                        this.peopleWorkersCoal -= this.peopleWorkersCoal / 10;
                        this.peopleWorkersFarm -= this.peopleWorkersFarm / 10;
                        this.peopleWorkersGold -= this.peopleWorkersGold / 10;
                        this.peopleHunters -= this.peopleHunters / 10;
                        this.peopleWorkersIron -= this.peopleWorkersIron / 10;
                        this.peopleIntelligence -= this.peopleIntelligence / 10;
                        this.peopleWorkers = this.peopleWorkersCoal + this.peopleWorkersFarm
                            + this.peopleWorkersGold + this.peopleWorkersIron + this.peopleFreeWorkers;
                        
                        if (timeWhenMessageBoxDoNotShow > 10)
                        {
                            applicationStop = true;
                            MessageBox.Show("Brakuje zborza, twoi ludzie umierają z głodu");
                            applicationStop = false;
                            timeWhenMessageBoxDoNotShow = 0;
                            this.updateGUI(true); // pełna aktualizacja GUI, bo jeżeli zginęli ludzie w kopalniach,
                                                  //to należy zaktualizować textboxy, które mówią o ilości ludzi w kopalniach
                        }

                    }
                    this.updateGUI(false); // niepełna aktualizacja GUI, bo aktualizujemy tylko surowce

                }
                if (timeWhenNewPeopleBorn > 2) // co dwie sekundy rodzą się ludzie
                {
                    double newPeople = allPeople * 0.1; // 10% obecnych ludzi przybywa co 2 sekundy
                    int newFighters = 0; // zmienne, określające nowych wojowników,
                    int newHunters = 0;  // pracowników, myśliwych i inteligencje,
                    int newWorkers = 0;  // która nie daje żadnych korzyści 
                    int newIntelligence = 0;
                    for (int i = 0; i < (int)newPeople; i++)
                    {
                        // w tej pentli za każdym razem losuje każdego nowo urudzonego człowieka
                        // kom zostanie w przyszłości
                        int randomNumber = rand.Next(1, 100);
                        if (randomNumber < 10)
                        {
                            newIntelligence++; // na inteligencje szansa 10%
                        }
                        else if (randomNumber < 30 && randomNumber >= 10)
                        {
                            newFighters++;// na wojownika szansa 20%
                        }
                        else if (randomNumber < 50 && randomNumber >= 30)
                        {
                            newHunters++;// na myśliwego szansa 20%
                        }
                        else if (randomNumber >= 50)
                        {
                            newWorkers++;// na robotnika szansa 50%
                        }
                    }
                    peopleWorkers += newWorkers; // zwiększam zmienne ilości poszczególnych 
                    peopleFreeWorkers += newWorkers; // grup społecznych o to, ile nowych
                    peopleIntelligence += newIntelligence; // ludzi się urodziło
                    peopleHunters += newHunters;
                    peopleFighters += newFighters;
                    timeWhenNewPeopleBorn = 0; // reset, aby ten blok wykonał się znowy za kolejne 2 sekundy
                    this.updateGUI(false); // aktualizacja GUI, teraz liczba osób jest inna, trzeba ją zaktualizować
                }               
            }
        }
        /// <summary>
        /// Przycisk, który wybiera wójta wojownika na starcie
        /// wójt wojownik ma większe szanse na wygrywanie bitew
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonChooseFighter_Click(object sender, EventArgs e)
        {
            leaderType = 1;
            chooseLeaderGUIChange();
        }
        /// <summary>
        /// Przycisk, który wybiera wójta ekonomiste na starcie
        /// wójt ekonomista zapewnia lepsze ceny w sklepie
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonChooseEconomist_Click(object sender, EventArgs e)
        {
            leaderType = 2;
            chooseLeaderGUIChange();
        }
        /// <summary>
        /// Przycisk, który wybiera wójta "neutralnego" na starcie
        /// wójt, który jest tym pomiędzy wojownikiem i ekonomistą
        /// nie ma żadnych zdolności.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonChooseMiddleLeader_Click(object sender, EventArgs e)
        {
            leaderType = 3;
            chooseLeaderGUIChange();
        }
        /// <summary>
        /// atakowanie pobliskiej wioski, kod wykonuje się po naciśnięciu na przycisk
        /// atakowania wioski można dzięki temu stracić całą armię, bądź zyskać nowe tereny
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAttackNearWillage_Click(object sender, EventArgs e)
        {
            if(peopleFighters<10) // jeżeli mamy poniżej 10 żołnierzy, to przegrywamy bitwę
            {
                this.iron = this.iron - peopleFighters; //Żołnierze walczący zużywają żelazo
                this.coal = this.coal - peopleFighters; //i węgiel
                if (coal < 0) //aby zapobiec ujemnym wartością, zeruje węgiel i żelazo, jeżeli będą mniejsze od zera
                {
                    coal = 0;
                }
                if (iron < 0)
                {
                    iron = 0;
                }
                applicationStop = true; // zmienna, która gdy true, blokuje wykonywanie się funkcji timer_Tick1
                MessageBox.Show("Przegrałeś, trzeba przynajmniej 10 żołnierzy...");
                applicationStop = false;
                this.peopleFighters = 0; // Gdy przegrywamy, tracimy wszystkich żołnierzy 
                updateGUI(false);
            }
            else // jeżeli mamy więcej niż 10 żołnierzy, bądź dokładnie 10
            {
                this.iron = this.iron - peopleFighters; // tutaj również zużywanie żelaza
                this.coal = this.coal - peopleFighters; // i węgla
                if (coal < 0)//aby zapobiec ujemnym wartością, zeruje węgiel i żelazo, jeżeli będą mniejsze od zera
                {
                    coal = 0;
                }
                if (iron < 0)
                {
                    iron = 0;
                }
                if (coal == 0) // jeżeli żelazo i węgiel są wyzerowane, oznacza to że ich brakło,
                {              // pojawia się więc komunikat o przegranej
                    applicationStop = true;// zmienna, która gdy true, blokuje wykonywanie się funkcji timer_Tick1
                    MessageBox.Show("Przegrałeś, brakło ci węgla na wyrób pocisków i broni...");
                    applicationStop = false;
                    this.peopleFighters = 0; // Gdy przegrywamy, tracimy wszystkich żołnierzy 
                    updateGUI(false); // aktualizuje GUI, bo zmienia się ilość żołnierzy
                }
                else if (iron == 0)
                {
                    applicationStop = true;// zmienna, która gdy true, blokuje wykonywanie się funkcji timer_Tick1
                    MessageBox.Show("Przegrałeś, brakło ci żelaza na wyrób pocisków i broni...");
                    applicationStop = false;
                    this.peopleFighters = 0; // Gdy przegrywamy, tracimy wszystkich żołnierzy 
                    updateGUI(false);// aktualizuje GUI, bo zmienia się ilość żołnierzy
                }
                else
                {
                    Random rand = new Random();
                    int random = rand.Next(1, 5);
                    int soldiersFromNearWillage = rand.Next(-100, 100); // losuje ilość żołnierzy w pobliskiej wiosce
                                       // jeżeli ilość żołnierzy jest ujemna, to zakładam, że wioska nie miała żołnierzy
                    if (leaderType == 1) //jeśli leaderType == 1 oznacza to że mamy przywódcę wojownika, który zapewnia lepszych żołnierzy
                    {
                        if (this.peopleFighters * 2 < soldiersFromNearWillage)  // Sprawdzam, czy naszego wojska wystarczy do pokonania wroga
                        {
                            applicationStop = true;// zmienna, która gdy true, blokuje wykonywanie się funkcji timer_Tick1
                            MessageBox.Show("Przegrałeś, oni mieli " + soldiersFromNearWillage.ToString() + " żołnierzy...");
                            applicationStop = false;
                            this.peopleFighters = 0; // Gdy przegrywamy, tracimy wszystkich żołnierzy 
                            updateGUI(false);// aktualizuje GUI, bo zmienia się ilość żołnierzy
                        }
                        else
                        {
                            int newHectars = rand.Next(1, 5); // losowanie, ile nowych hektarów ma tamta wioska
                            this.farmHectars += newHectars; // dodaje nowe hektary
                            if (soldiersFromNearWillage < 0)
                            {
                                soldiersFromNearWillage = 0;
                                applicationStop = true;// zmienna, która gdy true, blokuje wykonywanie się funkcji timer_Tick1
                                MessageBox.Show("Wygrałeś, pobliska wioska nie miała żołnierzy, zdobyłeś " + newHectars.ToString() + " hektarów pola.");
                                applicationStop = false;
                                updateGUI(false); // aktualizuje GUI, bo zmienia się ilość żołnierzy i hektarów
                            }
                            else
                            {
                                applicationStop = true;// zmienna, która gdy true, blokuje wykonywanie się funkcji timer_Tick1
                                MessageBox.Show("Wygrałeś, oni mieli " + soldiersFromNearWillage.ToString() + " żołnierzy, dzięki wójtowi wojakowi, jeden twój żołnież liczy się za dwóch ;)");
                                MessageBox.Show("Zdobyłeś " + newHectars.ToString() + " hektarów pola.");
                                applicationStop = false;
                                this.peopleFighters -= soldiersFromNearWillage / 2;
                                updateGUI(false);// aktualizuje GUI, bo zmienia się ilość hektarów i żołnierzy
                            }
                        }
                    }
                    else
                    {
                        if (this.peopleFighters < soldiersFromNearWillage)
                        {
                            applicationStop = true;// zmienna, która gdy true, blokuje wykonywanie się funkcji timer_Tick1
                            MessageBox.Show("Przegrałeś, oni mieli " + soldiersFromNearWillage.ToString() + " żołnierzy...");
                            applicationStop = false;
                            this.peopleFighters = 0; // Gdy przegrywamy, tracimy wszystkich żołnierzy 
                            updateGUI(false);// aktualizuje GUI, bo zmienia się ilość żołnierzy
                        }
                        else
                        {
                            int newHectars = rand.Next(1, 5); // losuje, ile nowych hektarów jest za wygranie bitwy
                            this.farmHectars += newHectars;// dodaje wylosowane hektary do puli
                            if (soldiersFromNearWillage < 0)
                            {
                                soldiersFromNearWillage = 0;
                                applicationStop = true;// zmienna, która gdy true, blokuje wykonywanie się funkcji timer_Tick1
                                MessageBox.Show("Wygrałeś, pobliska wioska nie miała żołnierzy, zdobyłeś " + newHectars.ToString() + " hektarów pola.");
                                applicationStop = false;
                                updateGUI(false);// aktualizuje GUI, bo zmienia się ilość hektarów
                            }
                            else
                            {
                                applicationStop = true;// zmienna, która gdy true, blokuje wykonywanie się funkcji timer_Tick1
                                MessageBox.Show("Wygrałeś, oni mieli " + soldiersFromNearWillage.ToString() + " żołnierzy, zdobyłeś " + newHectars.ToString() + " hektarów pola.");
                                applicationStop = false;
                                this.peopleFighters -= soldiersFromNearWillage;
                                updateGUI(false);// aktualizuje GUI, bo zmienia się ilość żołnierzy i hektarów
                            }
                        }
                    }  
                }
            }
        }
        /// <summary>
        /// funkcja, która wykonuje się pod przyciskiem handlowania
        /// uruchamia nowy form, w którym można kupować i sprzedawać,
        /// do konstruktora nowego forma przekazywana jest obecna ilość surowców
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonTrade_Click(object sender, EventArgs e)
        {
                                                //Argumenty dla konstruktora TradeForma
            Form myForm = new TradeForm(this, gold, iron, coal, foodMeat, foodGrain, leaderType);
            this.applicationStop = true; // zmienna, która gdy true, blokuje wykonywanie się funkcji timer_Tick1
            myForm.ShowDialog(); // show dialog zatrzymuje obecny kod i blokuje interakcje z obecnym oknem
            this.applicationStop = false;
        }
        /// <summary>
        /// Funkcja, która ustala ile ludzi ma pracować na farmach
        /// wykonuje się po naciśnięciu na przycisk "zatwierdź" 
        /// znajdujący się pod textboxem z pracownikami na polach
        /// Na farmach może pracować jedynie 10 osób na jednym hektarze pola
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFarmLandWorkersConfirm_Click(object sender, EventArgs e)
        {
            try
            { // konstrukcja try/catch umożliwia "przyłapanie" użytkownika na wpisywaniu liter
                //do textboxa, w którym planowaliśmy wpisywać liczby
                string newActualWorkersString = this.textBoxFarmWorkers.Text;
                int newActualWorkers = Int32.Parse(newActualWorkersString);
                //powyższe dwie linijki zczytują ciąg znaków z textBoxFarmWorkers i
                //umieszczają liczbę, która była tam zapisana w zmiennej intowej newActualWorkers
                if (this.peopleFreeWorkers >= (newActualWorkers - this.peopleWorkersFarm) && (farmHectars * 10) >= newActualWorkers) 
                {
                    // w tym ifie sprawdzam, czy użytkownik nie chce dać na farmy więcej ludzi, niż ich posiada
                    // lub też, czy nie chce dodać więcej ludzi niż maks 10 pracujących na hektar pola
                    // i jeżeli się zgadza, to zmniejszam ilość wolnych robotników o tyle, ile użytkownich chce
                    // i dodaje tę samą ilość do robotników pracujących na farmach
      
                    this.peopleFreeWorkers -= (newActualWorkers - this.peopleWorkersFarm);
                    this.peopleWorkersFarm = newActualWorkers;
                }
                else
                {
                    applicationStop = true; // zmienna, która gdy true, blokuje wykonywanie się funkcji timer_Tick1
                    MessageBox.Show("Albo nie masz pracowników, albo chcesz dać ich za dużo, maks 10 pracujących na hektar pola!!"); // jezeli użytkownik chce dać na farmy więcej lidzi niż ma dostępnych
                    applicationStop = false;
                }
            }
            catch(ArgumentNullException) // łapanie odpowiednich wyjątków i komentarz dla użytkownika w messageboxie
            {
                // jeżeli nie będzie stringa w textboxie 
                applicationStop = true;// zmienna, która gdy true, blokuje wykonywanie się funkcji timer_Tick1
                MessageBox.Show("Musisz podać wartość!");
                applicationStop = false;
            }
            catch(FormatException) // zły format, pusty tekst lub litera
            {
                if (this.textBoxFarmWorkers.Text == "")
                {
                    // jeżeli będzie string pusty w textboxie 
                    applicationStop = true;// zmienna, która gdy true, blokuje wykonywanie się funkcji timer_Tick1
                    MessageBox.Show("Musisz podać wartość!");
                    applicationStop = false;
                }
                else
                {
                    // jeżeli wpisano litere
                    applicationStop = true;// zmienna, która gdy true, blokuje wykonywanie się funkcji timer_Tick1
                    MessageBox.Show("Wpisz liczbę, a nie litery lub znaki!");
                    applicationStop = false;
                }
            }
            catch(OverflowException)
            {
                // jeżeli nastąpi przepełnienie, liczba będzie tak duża, że nie zmieścimy jej w 
                // typie int, również zgłaszamy to użytkownikowi
                applicationStop = true;// zmienna, która gdy true, blokuje wykonywanie się funkcji timer_Tick1
                MessageBox.Show("Za buża liczba!");
                applicationStop = false;
            }
        }
        /// <summary>
        /// Funkcja, która ustala ile ludzi ma pracować w kopalni złota
        /// wykonuje się po naciśnięciu na przycisk "zatwierdź" 
        /// znajdujący się pod textboxem z pracownikami w kopalni złota
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonGoldWorkersConfirm_Click(object sender, EventArgs e)
        {
            try
            { // okomentowanie obecnej funkcji jest identyczne jak buttonFarmLandWorkersConfirm_Click
                //czyli jednej funkcji powyżej, dlatego pozwalam je sobie pominąć
                string newActualWorkersString = this.textBoxGoldWorkers.Text;
                int newActualWorkers = Int32.Parse(newActualWorkersString);
                if (this.peopleFreeWorkers >= (newActualWorkers - this.peopleWorkersGold))
                {
                    this.peopleFreeWorkers -= (newActualWorkers - this.peopleWorkersGold);
                    this.peopleWorkersGold = newActualWorkers;
                }
                else
                {
                    applicationStop = true; // zmienna, która gdy true, blokuje wykonywanie się funkcji timer_Tick1
                    MessageBox.Show("Masz na to zbyt mało pracowników!"); // jezeli użytkownik chce dać na farmy więcej lidzi niż ma dostępnych
                    applicationStop = false;
                }
            }
            catch (ArgumentNullException)
            {
                applicationStop = true;
                MessageBox.Show("Musisz podać wartość!");
                applicationStop = false;
            }
            catch (FormatException)
            {
                if (this.textBoxFarmWorkers.Text == "")
                {
                    applicationStop = true;
                    MessageBox.Show("Musisz podać wartość!");
                    applicationStop = false;
                }
                else
                {
                    applicationStop = true;
                    MessageBox.Show("Wpisz liczbę, a nie litery lub znaki!");
                    applicationStop = false;
                }
            }
            catch (OverflowException)
            {
                applicationStop = true;
                MessageBox.Show("Za buża liczba!");
                applicationStop = false;
            }
        }
        /// <summary>
        /// Funkcja, która ustala ile ludzi ma pracować na kopalni żelaza
        /// wykonuje się po naciśnięciu na przycisk "zatwierdź" 
        /// znajdujący się pod textboxem z pracownikami kopalni żelaza
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonIronWorkersConfirm_Click(object sender, EventArgs e)
        {
            try
            {// okomentowanie obecnej funkcji jest identyczne jak buttonFarmLandWorkersConfirm_Click
                //czyli dwie funkcje powyżej, dlatego pozwalam je sobie pominąć
                string newActualWorkersString = this.textBoxIronWorkers.Text;
                int newActualWorkers = Int32.Parse(newActualWorkersString);
                if (this.peopleFreeWorkers >= (newActualWorkers - this.peopleWorkersIron))
                {
                    this.peopleFreeWorkers -= (newActualWorkers - this.peopleWorkersIron);
                    this.peopleWorkersIron = newActualWorkers;
                }
                else
                {
                    applicationStop = true; // zmienna, która gdy true, blokuje wykonywanie się funkcji timer_Tick1
                    MessageBox.Show("Masz na to zbyt mało pracowników!"); // jezeli użytkownik chce dać na farmy więcej lidzi niż ma dostępnych
                    applicationStop = false;
                }
            }
            catch (ArgumentNullException)
            {
                applicationStop = true;
                MessageBox.Show("Musisz podać wartość!");
                applicationStop = false;
            }
            catch (FormatException)
            {
                if (this.textBoxFarmWorkers.Text == "")
                {
                    applicationStop = true;
                    MessageBox.Show("Musisz podać wartość!");
                    applicationStop = false;
                }
                else
                {
                    applicationStop = true;
                    MessageBox.Show("Wpisz liczbę, a nie litery lub znaki!");
                    applicationStop = false;
                }
            }
            catch (OverflowException)
            {
                applicationStop = true;
                MessageBox.Show("Za buża liczba!");
                applicationStop = false;
            }
        }
        /// <summary>
        /// Funkcja, która ustala ile ludzi ma pracować na kopalni węgla
        /// wykonuje się po naciśnięciu na przycisk "zatwierdź" 
        /// znajdujący się pod textboxem z pracownikami kopalni węgla
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCoalWorkersConfirm_Click(object sender, EventArgs e)
        {
            try
            {// okomentowanie obecnej funkcji jest identyczne jak buttonFarmLandWorkersConfirm_Click
                //czyli trzy funkcje powyżej, dlatego pozwalam je sobie pominąć
                string newActualWorkersString = this.textBoxCoalWorkers.Text;
                int newActualWorkers = Int32.Parse(newActualWorkersString);
                if (this.peopleFreeWorkers >= (newActualWorkers - this.peopleWorkersCoal))
                {
                    this.peopleFreeWorkers -= (newActualWorkers - this.peopleWorkersCoal);
                    this.peopleWorkersCoal = newActualWorkers;
                }
                else
                {
                    applicationStop = true; // zmienna, która gdy true, blokuje wykonywanie się funkcji timer_Tick1
                    MessageBox.Show("Masz na to zbyt mało pracowników!"); // jezeli użytkownik chce dać na farmy więcej lidzi niż ma dostępnych
                    applicationStop = false;
                }
            }
            catch (ArgumentNullException)
            {
                applicationStop = true;
                MessageBox.Show("Musisz podać wartość!");
                applicationStop = false;
            }
            catch (FormatException)
            {
                if (this.textBoxFarmWorkers.Text == "")
                {
                    applicationStop = true;
                    MessageBox.Show("Musisz podać wartość!");
                    applicationStop = false;
                }
                else
                {
                    applicationStop = true;
                    MessageBox.Show("Wpisz liczbę, a nie litery lub znaki!");
                    applicationStop = false;
                }
            }
            catch (OverflowException)
            {
                applicationStop = true;
                MessageBox.Show("Za buża liczba!");
                applicationStop = false;
            }
        }
        /// <summary>
        /// Zamykanie programu, kod pod przyciskiem wyjścia
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonExit_Click(object sender, EventArgs e)
        {
            Close();
        }
        /// <summary>
        /// Dodatkowo, jeżeli ktoś naciśnie na form, pełne odświerzanie widoku, 
        /// funkcja przydaje się, gdy ktoś wprowadzi dane w textboxach, ale
        /// nie zatwierdza
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickOnForm(object sender, EventArgs e)
        {
            this.updateGUI(true);
        }
    }
}