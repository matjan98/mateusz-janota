﻿namespace MateuszJanotaZadanieDomoweLab1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelHectars = new System.Windows.Forms.Label();
            this.labelPopulation = new System.Windows.Forms.Label();
            this.labelGold = new System.Windows.Forms.Label();
            this.labelCoal = new System.Windows.Forms.Label();
            this.labelFoodGrain = new System.Windows.Forms.Label();
            this.labelIron = new System.Windows.Forms.Label();
            this.labelFoodMeat = new System.Windows.Forms.Label();
            this.labelHectarsAmount = new System.Windows.Forms.Label();
            this.labelPopulationAmount = new System.Windows.Forms.Label();
            this.labelIronAmount = new System.Windows.Forms.Label();
            this.labelGoldAmount = new System.Windows.Forms.Label();
            this.labelCoalAmount = new System.Windows.Forms.Label();
            this.labelFoodGrainAmount = new System.Windows.Forms.Label();
            this.labelFoodMeatAmount = new System.Windows.Forms.Label();
            this.labelFoodMeatWorkers = new System.Windows.Forms.Label();
            this.labelFoodGrainWorkers = new System.Windows.Forms.Label();
            this.textBoxFarmWorkers = new System.Windows.Forms.TextBox();
            this.textBoxGoldWorkers = new System.Windows.Forms.TextBox();
            this.textBoxIronWorkers = new System.Windows.Forms.TextBox();
            this.textBoxCoalWorkers = new System.Windows.Forms.TextBox();
            this.labelInfoMaterial = new System.Windows.Forms.Label();
            this.labelInfoWorkers = new System.Windows.Forms.Label();
            this.labelInfoAmount = new System.Windows.Forms.Label();
            this.labelWorkers = new System.Windows.Forms.Label();
            this.labelFighters = new System.Windows.Forms.Label();
            this.labelIntelligence = new System.Windows.Forms.Label();
            this.labelHunters = new System.Windows.Forms.Label();
            this.labelWorkersAmount = new System.Windows.Forms.Label();
            this.labelFightersAmount = new System.Windows.Forms.Label();
            this.labelIntelligenceAmount = new System.Windows.Forms.Label();
            this.labelHuntersAmount = new System.Windows.Forms.Label();
            this.buttonFarmLandWorkersConfirm = new System.Windows.Forms.Button();
            this.buttonGoldWorkersConfirm = new System.Windows.Forms.Button();
            this.buttonIronWorkersConfirm = new System.Windows.Forms.Button();
            this.buttonCoalWorkersConfirm = new System.Windows.Forms.Button();
            this.labelFreeWorkersInfo = new System.Windows.Forms.Label();
            this.labelFreeWorkersAmount = new System.Windows.Forms.Label();
            this.buttonAttackNearWillage = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.buttonTrade = new System.Windows.Forms.Button();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.labelDecriptionMiddleLeader = new System.Windows.Forms.Label();
            this.labelDectriptionEconomistLeader = new System.Windows.Forms.Label();
            this.labelDescriptionFighter = new System.Windows.Forms.Label();
            this.buttonChooseMiddleLeader = new System.Windows.Forms.Button();
            this.buttonChooseEconomist = new System.Windows.Forms.Button();
            this.buttonChooseFighter = new System.Windows.Forms.Button();
            this.labelChooseLeade = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelHectars
            // 
            this.labelHectars.AutoSize = true;
            this.labelHectars.Location = new System.Drawing.Point(93, 9);
            this.labelHectars.Name = "labelHectars";
            this.labelHectars.Size = new System.Drawing.Size(67, 13);
            this.labelHectars.TabIndex = 0;
            this.labelHectars.Text = "Hektary pola";
            this.labelHectars.Visible = false;
            // 
            // labelPopulation
            // 
            this.labelPopulation.AutoSize = true;
            this.labelPopulation.Location = new System.Drawing.Point(216, 9);
            this.labelPopulation.Name = "labelPopulation";
            this.labelPopulation.Size = new System.Drawing.Size(48, 13);
            this.labelPopulation.TabIndex = 1;
            this.labelPopulation.Text = "Ludność";
            this.labelPopulation.Visible = false;
            // 
            // labelGold
            // 
            this.labelGold.AutoSize = true;
            this.labelGold.BackColor = System.Drawing.SystemColors.Control;
            this.labelGold.Location = new System.Drawing.Point(323, 9);
            this.labelGold.Name = "labelGold";
            this.labelGold.Size = new System.Drawing.Size(33, 13);
            this.labelGold.TabIndex = 2;
            this.labelGold.Text = "Złoto";
            this.labelGold.Visible = false;
            // 
            // labelCoal
            // 
            this.labelCoal.AutoSize = true;
            this.labelCoal.Location = new System.Drawing.Point(499, 9);
            this.labelCoal.Name = "labelCoal";
            this.labelCoal.Size = new System.Drawing.Size(40, 13);
            this.labelCoal.TabIndex = 3;
            this.labelCoal.Text = "Węgiel";
            this.labelCoal.Visible = false;
            // 
            // labelFoodGrain
            // 
            this.labelFoodGrain.AutoSize = true;
            this.labelFoodGrain.Location = new System.Drawing.Point(598, 9);
            this.labelFoodGrain.Name = "labelFoodGrain";
            this.labelFoodGrain.Size = new System.Drawing.Size(37, 13);
            this.labelFoodGrain.TabIndex = 4;
            this.labelFoodGrain.Text = "Zboże";
            this.labelFoodGrain.Visible = false;
            // 
            // labelIron
            // 
            this.labelIron.AutoSize = true;
            this.labelIron.Location = new System.Drawing.Point(408, 9);
            this.labelIron.Name = "labelIron";
            this.labelIron.Size = new System.Drawing.Size(39, 13);
            this.labelIron.TabIndex = 5;
            this.labelIron.Text = "Żelazo";
            this.labelIron.Visible = false;
            // 
            // labelFoodMeat
            // 
            this.labelFoodMeat.AutoSize = true;
            this.labelFoodMeat.Location = new System.Drawing.Point(688, 9);
            this.labelFoodMeat.Name = "labelFoodMeat";
            this.labelFoodMeat.Size = new System.Drawing.Size(35, 13);
            this.labelFoodMeat.TabIndex = 6;
            this.labelFoodMeat.Text = "Mięso";
            this.labelFoodMeat.Visible = false;
            // 
            // labelHectarsAmount
            // 
            this.labelHectarsAmount.AutoSize = true;
            this.labelHectarsAmount.Location = new System.Drawing.Point(93, 31);
            this.labelHectarsAmount.Name = "labelHectarsAmount";
            this.labelHectarsAmount.Size = new System.Drawing.Size(13, 13);
            this.labelHectarsAmount.TabIndex = 7;
            this.labelHectarsAmount.Text = "1";
            this.labelHectarsAmount.Visible = false;
            // 
            // labelPopulationAmount
            // 
            this.labelPopulationAmount.AutoSize = true;
            this.labelPopulationAmount.Location = new System.Drawing.Point(216, 31);
            this.labelPopulationAmount.Name = "labelPopulationAmount";
            this.labelPopulationAmount.Size = new System.Drawing.Size(19, 13);
            this.labelPopulationAmount.TabIndex = 8;
            this.labelPopulationAmount.Text = "10";
            this.labelPopulationAmount.Visible = false;
            // 
            // labelIronAmount
            // 
            this.labelIronAmount.AutoSize = true;
            this.labelIronAmount.Location = new System.Drawing.Point(410, 31);
            this.labelIronAmount.Name = "labelIronAmount";
            this.labelIronAmount.Size = new System.Drawing.Size(31, 13);
            this.labelIronAmount.TabIndex = 9;
            this.labelIronAmount.Text = "1000";
            this.labelIronAmount.Visible = false;
            // 
            // labelGoldAmount
            // 
            this.labelGoldAmount.AutoSize = true;
            this.labelGoldAmount.Location = new System.Drawing.Point(324, 31);
            this.labelGoldAmount.Name = "labelGoldAmount";
            this.labelGoldAmount.Size = new System.Drawing.Size(31, 13);
            this.labelGoldAmount.TabIndex = 10;
            this.labelGoldAmount.Text = "1000";
            this.labelGoldAmount.Visible = false;
            // 
            // labelCoalAmount
            // 
            this.labelCoalAmount.AutoSize = true;
            this.labelCoalAmount.Location = new System.Drawing.Point(501, 31);
            this.labelCoalAmount.Name = "labelCoalAmount";
            this.labelCoalAmount.Size = new System.Drawing.Size(31, 13);
            this.labelCoalAmount.TabIndex = 11;
            this.labelCoalAmount.Text = "1000";
            this.labelCoalAmount.Visible = false;
            // 
            // labelFoodGrainAmount
            // 
            this.labelFoodGrainAmount.AutoSize = true;
            this.labelFoodGrainAmount.Location = new System.Drawing.Point(598, 31);
            this.labelFoodGrainAmount.Name = "labelFoodGrainAmount";
            this.labelFoodGrainAmount.Size = new System.Drawing.Size(25, 13);
            this.labelFoodGrainAmount.TabIndex = 12;
            this.labelFoodGrainAmount.Text = "100";
            this.labelFoodGrainAmount.Visible = false;
            // 
            // labelFoodMeatAmount
            // 
            this.labelFoodMeatAmount.AutoSize = true;
            this.labelFoodMeatAmount.Location = new System.Drawing.Point(691, 31);
            this.labelFoodMeatAmount.Name = "labelFoodMeatAmount";
            this.labelFoodMeatAmount.Size = new System.Drawing.Size(25, 13);
            this.labelFoodMeatAmount.TabIndex = 13;
            this.labelFoodMeatAmount.Text = "100";
            this.labelFoodMeatAmount.Visible = false;
            // 
            // labelFoodMeatWorkers
            // 
            this.labelFoodMeatWorkers.AutoSize = true;
            this.labelFoodMeatWorkers.Location = new System.Drawing.Point(691, 58);
            this.labelFoodMeatWorkers.Name = "labelFoodMeatWorkers";
            this.labelFoodMeatWorkers.Size = new System.Drawing.Size(13, 13);
            this.labelFoodMeatWorkers.TabIndex = 15;
            this.labelFoodMeatWorkers.Text = "0";
            this.labelFoodMeatWorkers.Visible = false;
            // 
            // labelFoodGrainWorkers
            // 
            this.labelFoodGrainWorkers.AutoSize = true;
            this.labelFoodGrainWorkers.Location = new System.Drawing.Point(598, 58);
            this.labelFoodGrainWorkers.Name = "labelFoodGrainWorkers";
            this.labelFoodGrainWorkers.Size = new System.Drawing.Size(13, 13);
            this.labelFoodGrainWorkers.TabIndex = 14;
            this.labelFoodGrainWorkers.Text = "0";
            this.labelFoodGrainWorkers.Visible = false;
            // 
            // textBoxFarmWorkers
            // 
            this.textBoxFarmWorkers.Location = new System.Drawing.Point(93, 55);
            this.textBoxFarmWorkers.Name = "textBoxFarmWorkers";
            this.textBoxFarmWorkers.Size = new System.Drawing.Size(67, 20);
            this.textBoxFarmWorkers.TabIndex = 16;
            this.textBoxFarmWorkers.Text = "0";
            this.textBoxFarmWorkers.Visible = false;
            // 
            // textBoxGoldWorkers
            // 
            this.textBoxGoldWorkers.Location = new System.Drawing.Point(325, 55);
            this.textBoxGoldWorkers.Name = "textBoxGoldWorkers";
            this.textBoxGoldWorkers.Size = new System.Drawing.Size(68, 20);
            this.textBoxGoldWorkers.TabIndex = 18;
            this.textBoxGoldWorkers.Text = "0";
            this.textBoxGoldWorkers.Visible = false;
            // 
            // textBoxIronWorkers
            // 
            this.textBoxIronWorkers.Location = new System.Drawing.Point(411, 55);
            this.textBoxIronWorkers.Name = "textBoxIronWorkers";
            this.textBoxIronWorkers.Size = new System.Drawing.Size(68, 20);
            this.textBoxIronWorkers.TabIndex = 19;
            this.textBoxIronWorkers.Text = "0";
            this.textBoxIronWorkers.Visible = false;
            // 
            // textBoxCoalWorkers
            // 
            this.textBoxCoalWorkers.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxCoalWorkers.Location = new System.Drawing.Point(498, 55);
            this.textBoxCoalWorkers.Name = "textBoxCoalWorkers";
            this.textBoxCoalWorkers.Size = new System.Drawing.Size(68, 20);
            this.textBoxCoalWorkers.TabIndex = 20;
            this.textBoxCoalWorkers.Text = "0";
            this.textBoxCoalWorkers.Visible = false;
            // 
            // labelInfoMaterial
            // 
            this.labelInfoMaterial.AutoSize = true;
            this.labelInfoMaterial.Location = new System.Drawing.Point(12, 9);
            this.labelInfoMaterial.Name = "labelInfoMaterial";
            this.labelInfoMaterial.Size = new System.Drawing.Size(54, 13);
            this.labelInfoMaterial.TabIndex = 21;
            this.labelInfoMaterial.Text = "Surowiec:";
            this.labelInfoMaterial.Visible = false;
            // 
            // labelInfoWorkers
            // 
            this.labelInfoWorkers.AutoSize = true;
            this.labelInfoWorkers.Location = new System.Drawing.Point(12, 58);
            this.labelInfoWorkers.Name = "labelInfoWorkers";
            this.labelInfoWorkers.Size = new System.Drawing.Size(65, 13);
            this.labelInfoWorkers.TabIndex = 23;
            this.labelInfoWorkers.Text = "Pracownicy:";
            this.labelInfoWorkers.Visible = false;
            // 
            // labelInfoAmount
            // 
            this.labelInfoAmount.AutoSize = true;
            this.labelInfoAmount.Location = new System.Drawing.Point(12, 31);
            this.labelInfoAmount.Name = "labelInfoAmount";
            this.labelInfoAmount.Size = new System.Drawing.Size(75, 13);
            this.labelInfoAmount.TabIndex = 24;
            this.labelInfoAmount.Text = "Ilość surowca:";
            this.labelInfoAmount.Visible = false;
            // 
            // labelWorkers
            // 
            this.labelWorkers.AutoSize = true;
            this.labelWorkers.Location = new System.Drawing.Point(190, 55);
            this.labelWorkers.Name = "labelWorkers";
            this.labelWorkers.Size = new System.Drawing.Size(41, 13);
            this.labelWorkers.TabIndex = 25;
            this.labelWorkers.Text = "Chłopi:";
            this.labelWorkers.Visible = false;
            // 
            // labelFighters
            // 
            this.labelFighters.AutoSize = true;
            this.labelFighters.Location = new System.Drawing.Point(190, 68);
            this.labelFighters.Name = "labelFighters";
            this.labelFighters.Size = new System.Drawing.Size(46, 13);
            this.labelFighters.TabIndex = 26;
            this.labelFighters.Text = "Wojacy:";
            this.labelFighters.Visible = false;
            // 
            // labelIntelligence
            // 
            this.labelIntelligence.AutoSize = true;
            this.labelIntelligence.Location = new System.Drawing.Point(190, 81);
            this.labelIntelligence.Name = "labelIntelligence";
            this.labelIntelligence.Size = new System.Drawing.Size(64, 13);
            this.labelIntelligence.TabIndex = 27;
            this.labelIntelligence.Text = "Inteligencja:";
            this.labelIntelligence.Visible = false;
            // 
            // labelHunters
            // 
            this.labelHunters.AutoSize = true;
            this.labelHunters.Location = new System.Drawing.Point(190, 94);
            this.labelHunters.Name = "labelHunters";
            this.labelHunters.Size = new System.Drawing.Size(43, 13);
            this.labelHunters.TabIndex = 28;
            this.labelHunters.Text = "Myśliwi:";
            this.labelHunters.Visible = false;
            // 
            // labelWorkersAmount
            // 
            this.labelWorkersAmount.AutoSize = true;
            this.labelWorkersAmount.Location = new System.Drawing.Point(228, 55);
            this.labelWorkersAmount.Name = "labelWorkersAmount";
            this.labelWorkersAmount.Size = new System.Drawing.Size(19, 13);
            this.labelWorkersAmount.TabIndex = 29;
            this.labelWorkersAmount.Text = "10";
            this.labelWorkersAmount.Visible = false;
            // 
            // labelFightersAmount
            // 
            this.labelFightersAmount.AutoSize = true;
            this.labelFightersAmount.Location = new System.Drawing.Point(233, 68);
            this.labelFightersAmount.Name = "labelFightersAmount";
            this.labelFightersAmount.Size = new System.Drawing.Size(13, 13);
            this.labelFightersAmount.TabIndex = 30;
            this.labelFightersAmount.Text = "0";
            this.labelFightersAmount.Visible = false;
            // 
            // labelIntelligenceAmount
            // 
            this.labelIntelligenceAmount.AutoSize = true;
            this.labelIntelligenceAmount.Location = new System.Drawing.Point(251, 81);
            this.labelIntelligenceAmount.Name = "labelIntelligenceAmount";
            this.labelIntelligenceAmount.Size = new System.Drawing.Size(13, 13);
            this.labelIntelligenceAmount.TabIndex = 31;
            this.labelIntelligenceAmount.Text = "0";
            this.labelIntelligenceAmount.Visible = false;
            // 
            // labelHuntersAmount
            // 
            this.labelHuntersAmount.AutoSize = true;
            this.labelHuntersAmount.Location = new System.Drawing.Point(230, 94);
            this.labelHuntersAmount.Name = "labelHuntersAmount";
            this.labelHuntersAmount.Size = new System.Drawing.Size(13, 13);
            this.labelHuntersAmount.TabIndex = 32;
            this.labelHuntersAmount.Text = "0";
            this.labelHuntersAmount.Visible = false;
            // 
            // buttonFarmLandWorkersConfirm
            // 
            this.buttonFarmLandWorkersConfirm.Location = new System.Drawing.Point(93, 82);
            this.buttonFarmLandWorkersConfirm.Name = "buttonFarmLandWorkersConfirm";
            this.buttonFarmLandWorkersConfirm.Size = new System.Drawing.Size(67, 23);
            this.buttonFarmLandWorkersConfirm.TabIndex = 33;
            this.buttonFarmLandWorkersConfirm.Text = "Zatwierdź";
            this.buttonFarmLandWorkersConfirm.UseVisualStyleBackColor = true;
            this.buttonFarmLandWorkersConfirm.Visible = false;
            this.buttonFarmLandWorkersConfirm.Click += new System.EventHandler(this.buttonFarmLandWorkersConfirm_Click);
            // 
            // buttonGoldWorkersConfirm
            // 
            this.buttonGoldWorkersConfirm.Location = new System.Drawing.Point(325, 84);
            this.buttonGoldWorkersConfirm.Name = "buttonGoldWorkersConfirm";
            this.buttonGoldWorkersConfirm.Size = new System.Drawing.Size(68, 23);
            this.buttonGoldWorkersConfirm.TabIndex = 34;
            this.buttonGoldWorkersConfirm.Text = "Zatwierdź";
            this.buttonGoldWorkersConfirm.UseVisualStyleBackColor = true;
            this.buttonGoldWorkersConfirm.Visible = false;
            this.buttonGoldWorkersConfirm.Click += new System.EventHandler(this.buttonGoldWorkersConfirm_Click);
            // 
            // buttonIronWorkersConfirm
            // 
            this.buttonIronWorkersConfirm.Location = new System.Drawing.Point(411, 84);
            this.buttonIronWorkersConfirm.Name = "buttonIronWorkersConfirm";
            this.buttonIronWorkersConfirm.Size = new System.Drawing.Size(68, 23);
            this.buttonIronWorkersConfirm.TabIndex = 35;
            this.buttonIronWorkersConfirm.Text = "Zatwierdź";
            this.buttonIronWorkersConfirm.UseVisualStyleBackColor = true;
            this.buttonIronWorkersConfirm.Visible = false;
            this.buttonIronWorkersConfirm.Click += new System.EventHandler(this.buttonIronWorkersConfirm_Click);
            // 
            // buttonCoalWorkersConfirm
            // 
            this.buttonCoalWorkersConfirm.Location = new System.Drawing.Point(498, 84);
            this.buttonCoalWorkersConfirm.Name = "buttonCoalWorkersConfirm";
            this.buttonCoalWorkersConfirm.Size = new System.Drawing.Size(68, 23);
            this.buttonCoalWorkersConfirm.TabIndex = 36;
            this.buttonCoalWorkersConfirm.Text = "Zatwierdź";
            this.buttonCoalWorkersConfirm.UseVisualStyleBackColor = true;
            this.buttonCoalWorkersConfirm.Visible = false;
            this.buttonCoalWorkersConfirm.Click += new System.EventHandler(this.buttonCoalWorkersConfirm_Click);
            // 
            // labelFreeWorkersInfo
            // 
            this.labelFreeWorkersInfo.AutoSize = true;
            this.labelFreeWorkersInfo.Location = new System.Drawing.Point(588, 89);
            this.labelFreeWorkersInfo.Name = "labelFreeWorkersInfo";
            this.labelFreeWorkersInfo.Size = new System.Drawing.Size(70, 13);
            this.labelFreeWorkersInfo.TabIndex = 37;
            this.labelFreeWorkersInfo.Text = "Wolni chłopi:";
            this.labelFreeWorkersInfo.Visible = false;
            // 
            // labelFreeWorkersAmount
            // 
            this.labelFreeWorkersAmount.AutoSize = true;
            this.labelFreeWorkersAmount.Location = new System.Drawing.Point(655, 89);
            this.labelFreeWorkersAmount.Name = "labelFreeWorkersAmount";
            this.labelFreeWorkersAmount.Size = new System.Drawing.Size(19, 13);
            this.labelFreeWorkersAmount.TabIndex = 38;
            this.labelFreeWorkersAmount.Text = "10";
            this.labelFreeWorkersAmount.Visible = false;
            // 
            // buttonAttackNearWillage
            // 
            this.buttonAttackNearWillage.Location = new System.Drawing.Point(12, 233);
            this.buttonAttackNearWillage.Name = "buttonAttackNearWillage";
            this.buttonAttackNearWillage.Size = new System.Drawing.Size(294, 23);
            this.buttonAttackNearWillage.TabIndex = 39;
            this.buttonAttackNearWillage.Text = "Podbij pobliską wioskę, aby zdobyć więcej pola";
            this.buttonAttackNearWillage.UseVisualStyleBackColor = true;
            this.buttonAttackNearWillage.Visible = false;
            this.buttonAttackNearWillage.Click += new System.EventHandler(this.buttonAttackNearWillage_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(312, 233);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(122, 23);
            this.buttonExit.TabIndex = 40;
            this.buttonExit.Text = "Wyjście z programu";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Visible = false;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // buttonTrade
            // 
            this.buttonTrade.Location = new System.Drawing.Point(440, 233);
            this.buttonTrade.Name = "buttonTrade";
            this.buttonTrade.Size = new System.Drawing.Size(292, 23);
            this.buttonTrade.TabIndex = 41;
            this.buttonTrade.Text = "Handluj towarem";
            this.buttonTrade.UseVisualStyleBackColor = true;
            this.buttonTrade.Visible = false;
            this.buttonTrade.Click += new System.EventHandler(this.buttonTrade_Click);
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick_1);
            // 
            // labelDecriptionMiddleLeader
            // 
            this.labelDecriptionMiddleLeader.AutoSize = true;
            this.labelDecriptionMiddleLeader.Location = new System.Drawing.Point(545, 208);
            this.labelDecriptionMiddleLeader.Name = "labelDecriptionMiddleLeader";
            this.labelDecriptionMiddleLeader.Size = new System.Drawing.Size(177, 13);
            this.labelDecriptionMiddleLeader.TabIndex = 55;
            this.labelDecriptionMiddleLeader.Text = "Koleś potrafi wszystko... po trochu...";
            // 
            // labelDectriptionEconomistLeader
            // 
            this.labelDectriptionEconomistLeader.AutoSize = true;
            this.labelDectriptionEconomistLeader.Location = new System.Drawing.Point(309, 208);
            this.labelDectriptionEconomistLeader.Name = "labelDectriptionEconomistLeader";
            this.labelDectriptionEconomistLeader.Size = new System.Drawing.Size(125, 13);
            this.labelDectriptionEconomistLeader.TabIndex = 54;
            this.labelDectriptionEconomistLeader.Text = "Zawsze pilnie się uczył...";
            // 
            // labelDescriptionFighter
            // 
            this.labelDescriptionFighter.AutoSize = true;
            this.labelDescriptionFighter.Location = new System.Drawing.Point(15, 208);
            this.labelDescriptionFighter.Name = "labelDescriptionFighter";
            this.labelDescriptionFighter.Size = new System.Drawing.Size(237, 13);
            this.labelDescriptionFighter.TabIndex = 53;
            this.labelDescriptionFighter.Text = "Ponoć kiedyś zabił niedźwiedzia gołymi rękami...";
            // 
            // buttonChooseMiddleLeader
            // 
            this.buttonChooseMiddleLeader.Location = new System.Drawing.Point(530, 125);
            this.buttonChooseMiddleLeader.Name = "buttonChooseMiddleLeader";
            this.buttonChooseMiddleLeader.Size = new System.Drawing.Size(202, 76);
            this.buttonChooseMiddleLeader.TabIndex = 52;
            this.buttonChooseMiddleLeader.Text = "Wójt renesansu";
            this.buttonChooseMiddleLeader.UseVisualStyleBackColor = true;
            this.buttonChooseMiddleLeader.Click += new System.EventHandler(this.buttonChooseMiddleLeader_Click);
            // 
            // buttonChooseEconomist
            // 
            this.buttonChooseEconomist.Location = new System.Drawing.Point(271, 125);
            this.buttonChooseEconomist.Name = "buttonChooseEconomist";
            this.buttonChooseEconomist.Size = new System.Drawing.Size(202, 76);
            this.buttonChooseEconomist.TabIndex = 51;
            this.buttonChooseEconomist.Text = "Wójt ekonomista!";
            this.buttonChooseEconomist.UseVisualStyleBackColor = true;
            this.buttonChooseEconomist.Click += new System.EventHandler(this.buttonChooseEconomist_Click);
            // 
            // buttonChooseFighter
            // 
            this.buttonChooseFighter.Location = new System.Drawing.Point(15, 125);
            this.buttonChooseFighter.Name = "buttonChooseFighter";
            this.buttonChooseFighter.Size = new System.Drawing.Size(202, 76);
            this.buttonChooseFighter.TabIndex = 50;
            this.buttonChooseFighter.Text = "Potężny wojak!";
            this.buttonChooseFighter.UseVisualStyleBackColor = true;
            this.buttonChooseFighter.Click += new System.EventHandler(this.buttonChooseFighter_Click);
            // 
            // labelChooseLeade
            // 
            this.labelChooseLeade.AutoSize = true;
            this.labelChooseLeade.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelChooseLeade.Location = new System.Drawing.Point(182, 22);
            this.labelChooseLeade.Name = "labelChooseLeade";
            this.labelChooseLeade.Size = new System.Drawing.Size(379, 63);
            this.labelChooseLeade.TabIndex = 49;
            this.labelChooseLeade.Text = "Wybierz wójta:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 268);
            this.Controls.Add(this.labelDecriptionMiddleLeader);
            this.Controls.Add(this.labelDectriptionEconomistLeader);
            this.Controls.Add(this.labelDescriptionFighter);
            this.Controls.Add(this.buttonChooseMiddleLeader);
            this.Controls.Add(this.buttonChooseEconomist);
            this.Controls.Add(this.buttonChooseFighter);
            this.Controls.Add(this.labelChooseLeade);
            this.Controls.Add(this.buttonTrade);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.buttonAttackNearWillage);
            this.Controls.Add(this.labelFreeWorkersAmount);
            this.Controls.Add(this.labelFreeWorkersInfo);
            this.Controls.Add(this.buttonCoalWorkersConfirm);
            this.Controls.Add(this.buttonIronWorkersConfirm);
            this.Controls.Add(this.buttonGoldWorkersConfirm);
            this.Controls.Add(this.buttonFarmLandWorkersConfirm);
            this.Controls.Add(this.labelHuntersAmount);
            this.Controls.Add(this.labelIntelligenceAmount);
            this.Controls.Add(this.labelFightersAmount);
            this.Controls.Add(this.labelWorkersAmount);
            this.Controls.Add(this.labelHunters);
            this.Controls.Add(this.labelIntelligence);
            this.Controls.Add(this.labelFighters);
            this.Controls.Add(this.labelWorkers);
            this.Controls.Add(this.labelInfoAmount);
            this.Controls.Add(this.labelInfoWorkers);
            this.Controls.Add(this.labelInfoMaterial);
            this.Controls.Add(this.textBoxCoalWorkers);
            this.Controls.Add(this.textBoxIronWorkers);
            this.Controls.Add(this.textBoxGoldWorkers);
            this.Controls.Add(this.textBoxFarmWorkers);
            this.Controls.Add(this.labelFoodMeatWorkers);
            this.Controls.Add(this.labelFoodGrainWorkers);
            this.Controls.Add(this.labelFoodMeatAmount);
            this.Controls.Add(this.labelFoodGrainAmount);
            this.Controls.Add(this.labelCoalAmount);
            this.Controls.Add(this.labelGoldAmount);
            this.Controls.Add(this.labelIronAmount);
            this.Controls.Add(this.labelPopulationAmount);
            this.Controls.Add(this.labelHectarsAmount);
            this.Controls.Add(this.labelFoodMeat);
            this.Controls.Add(this.labelIron);
            this.Controls.Add(this.labelFoodGrain);
            this.Controls.Add(this.labelCoal);
            this.Controls.Add(this.labelGold);
            this.Controls.Add(this.labelPopulation);
            this.Controls.Add(this.labelHectars);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainForm";
            this.Text = "Twoja wioska";
            this.Click += new System.EventHandler(this.ClickOnForm);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelHectars;
        private System.Windows.Forms.Label labelPopulation;
        private System.Windows.Forms.Label labelGold;
        private System.Windows.Forms.Label labelCoal;
        private System.Windows.Forms.Label labelFoodGrain;
        private System.Windows.Forms.Label labelIron;
        private System.Windows.Forms.Label labelFoodMeat;
        private System.Windows.Forms.Label labelHectarsAmount;
        private System.Windows.Forms.Label labelPopulationAmount;
        private System.Windows.Forms.Label labelIronAmount;
        private System.Windows.Forms.Label labelGoldAmount;
        private System.Windows.Forms.Label labelCoalAmount;
        private System.Windows.Forms.Label labelFoodGrainAmount;
        private System.Windows.Forms.Label labelFoodMeatAmount;
        private System.Windows.Forms.Label labelFoodMeatWorkers;
        private System.Windows.Forms.Label labelFoodGrainWorkers;
        private System.Windows.Forms.TextBox textBoxFarmWorkers;
        private System.Windows.Forms.TextBox textBoxGoldWorkers;
        private System.Windows.Forms.TextBox textBoxIronWorkers;
        private System.Windows.Forms.TextBox textBoxCoalWorkers;
        private System.Windows.Forms.Label labelInfoMaterial;
        private System.Windows.Forms.Label labelInfoWorkers;
        private System.Windows.Forms.Label labelInfoAmount;
        private System.Windows.Forms.Label labelWorkers;
        private System.Windows.Forms.Label labelFighters;
        private System.Windows.Forms.Label labelIntelligence;
        private System.Windows.Forms.Label labelHunters;
        private System.Windows.Forms.Label labelWorkersAmount;
        private System.Windows.Forms.Label labelFightersAmount;
        private System.Windows.Forms.Label labelIntelligenceAmount;
        private System.Windows.Forms.Label labelHuntersAmount;
        private System.Windows.Forms.Button buttonFarmLandWorkersConfirm;
        private System.Windows.Forms.Button buttonGoldWorkersConfirm;
        private System.Windows.Forms.Button buttonIronWorkersConfirm;
        private System.Windows.Forms.Button buttonCoalWorkersConfirm;
        private System.Windows.Forms.Label labelFreeWorkersInfo;
        private System.Windows.Forms.Label labelFreeWorkersAmount;
        private System.Windows.Forms.Button buttonAttackNearWillage;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Button buttonTrade;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label labelDecriptionMiddleLeader;
        private System.Windows.Forms.Label labelDectriptionEconomistLeader;
        private System.Windows.Forms.Label labelDescriptionFighter;
        private System.Windows.Forms.Button buttonChooseMiddleLeader;
        private System.Windows.Forms.Button buttonChooseEconomist;
        private System.Windows.Forms.Button buttonChooseFighter;
        private System.Windows.Forms.Label labelChooseLeade;
    }
}

