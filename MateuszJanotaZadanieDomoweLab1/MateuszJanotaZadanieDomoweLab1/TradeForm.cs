﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MateuszJanotaZadanieDomoweLab1
{
    public partial class TradeForm : Form
    {
        int whatToDo; // Zmienia się po kliknięciu w przyciski kup/sprzedaj
                      // 1-4 - odpowiednio kupno żelaza, węgla, zborza, mięsa.
                      // 5-8 - odpowiednio sprzedarz żelaza, węgla, zborza, mięsa.

        int gold; // ilość obecnego złota
        int iron; // ilość obecnego żelaza
        int coal; // ilość obecnego węgla
        int foodMeat; // ilość obecnego mięsa
        int foodGrain; // ilość obecnego zborza

        double ironPriceBuy; // cena żelaza na kupno
        double coalPriceBuy; // cena węgla na kupno
        double foodMeatPriceBuy; // cena mięsa na kupno
        double foodGrainPriceBuy; // cena zboża na kupno

        double ironPriceSell; // cena żelaza na sprzedarz
        double coalPriceSell; // cena węgla na sprzedarz
        double foodMeatPriceSell; // cena mięsa na sprzedarz
        double foodGrainPriceSell; // cena zboża na sprzedaż

        MainForm parentReference;

        /// <summary>
        /// konstruktor klasy TradeForm
        /// przyjmuje on parametry, które oznaczają, ile posiadamy w stym momencie surowców
        /// każdy parametr to osobny surowiec, parametr parent jest potrzebny, aby użyć
        /// setterów i getterów klasy MainForm
        /// oraz leaderType to parametr, który określa jakiego mamy wójta (wójt ekonomista zapewnia lepsze ceny)
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="gold"></param>
        /// <param name="iron"></param>
        /// <param name="coal"></param>
        /// <param name="foodMeat"></param>
        /// <param name="foodGrain"></param>
        /// <param name="leaderType"></param>
        public TradeForm(MainForm parent, int gold, int iron, int coal, int foodMeat, int foodGrain, int leaderType)
        {
            InitializeComponent();

            //poniżej ustawiam tło wszystkich labeli w oknie na przeźroczysty
            labelBetterPrices.BackColor = Color.Transparent;
            labelCoal.BackColor = Color.Transparent;
            labelCoalAmount.BackColor = Color.Transparent;
            labelFoodGrain.BackColor = Color.Transparent;
            labelFoodGrainAmount.BackColor = Color.Transparent;
            labelFoodMeat.BackColor = Color.Transparent;
            labelFoodMeatAmount.BackColor = Color.Transparent;
            labelGold.BackColor = Color.Transparent;
            labelGoldAmount.BackColor = Color.Transparent;
            labelHowMuchSellOrBuy.BackColor = Color.Transparent;
            labelIron.BackColor = Color.Transparent;
            labelIronAmount.BackColor = Color.Transparent;
            labelPriceInGold.BackColor = Color.Transparent;

            this.gold = gold; // ustalam wszelkie zmienna surowców z tego okna, na takie, jakie otrzymałem w argumentach
            this.iron = iron; // ustalam wszelkie zmienna surowców z tego okna, na takie, jakie otrzymałem w argumentach
            this.foodMeat = foodMeat; // ustalam wszelkie zmienna surowców z tego okna, na takie, jakie otrzymałem w argumentach
            this.foodGrain = foodGrain; // ustalam wszelkie zmienna surowców z tego okna, na takie, jakie otrzymałem w argumentach
            this.coal = coal; // ustalam wszelkie zmienna surowców z tego okna, na takie, jakie otrzymałem w argumentach

            this.parentReference = parent; // parentReference to referencja na obiekt klasy
                                           // MainForm, dzięki której możemy używać setterów
                                           // i zmieniać ilość surówców

            whatToDo = 0; // zmienna może mieć wartości od 1 - 8. Ustawiam ją na 0, aby program wiedział
                          // że użytkownik nie wybrał jeszcze, co chce kupić / sprzedać

            if(leaderType == 2) // ustawiam lepsze przeliczniki, gdy jest wójt ekonomista
            {
                labelBetterPrices.Visible = true;
                ironPriceBuy = 1.4;
                coalPriceBuy = 1.1;
                foodMeatPriceBuy = 1.6;
                foodGrainPriceBuy = 0.6;

                ironPriceSell = 1.3;
                coalPriceSell = 1.0;
                foodMeatPriceSell = 1.4;
                foodGrainPriceSell = 0.5;
            }
            else // w innym wypadku zwykłe przeliczniki
            {
                ironPriceBuy = 1.5;
                coalPriceBuy = 1.2;
                foodMeatPriceBuy = 1.8;
                foodGrainPriceBuy = 0.7;

                ironPriceSell = 1.2;
                coalPriceSell = 1.0;
                foodMeatPriceSell = 1.3;
                foodGrainPriceSell = 0.4;
            }
            updateGUI(); // Aktualizuje ilość surowcó
        }

        /// <summary>
        /// Aktualizuje ilość surowców w labelach
        /// </summary>
        void updateGUI()
        {
            labelCoalAmount.Text = coal.ToString();
            labelFoodGrainAmount.Text = foodGrain.ToString();
            labelFoodMeatAmount.Text = foodMeat.ToString();
            labelGoldAmount.Text = gold.ToString();
            labelIronAmount.Text = iron.ToString();
        }


        /// <summary>
        /// Funkcja wykonywująca się pod przyciskiem zatwierdź
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAgree_Click(object sender, EventArgs e)
        {
            try
            {
                string howMuchString = this.textBoxHowMuchSellOrBuy.Text; // ile towaru chcemy kupić/sprzedać w stringu
                int howMuch = Int32.Parse(howMuchString); // ile towaru chcemy kupić/sprzedać w incie po sparsowaniu
                string howMuchInGoldString = this.textBoxPriceInGold.Text; // ile złota dostaniemy/wydamy w stringu
                int howMuchInGold = Int32.Parse(howMuchInGoldString); // ile złota dostaniemy/wydamy w incie po sparsowaniu
                if (howMuchInGold < gold && whatToDo < 5) // jeżeli mamy więcej złota niż ilość złota, które dostaniemy/wydamy                              
                {                                         // i zamierzamy coś kupić (whatToDo < 5), to wykonujemy odpowiednią transakcje
                    if (whatToDo == 1)                    // którą wybraliśmy wcześniej
                    {
                        gold -= howMuchInGold;
                        iron += howMuch;
                    }
                    if (whatToDo == 2)
                    {
                        gold -= howMuchInGold;
                        coal += howMuch;
                    }
                    if (whatToDo == 3)
                    {
                        gold -= howMuchInGold;
                        foodGrain += howMuch;
                    }
                    if (whatToDo == 4)
                    {
                        gold -= howMuchInGold;
                        foodMeat += howMuch;
                    }
                }
                else
                {
                    MessageBox.Show("Nie stać cię :(");
                }

                if (whatToDo == 5)
                {
                    if (iron > howMuch)
                    {
                        gold += howMuchInGold;
                        iron -= howMuch;
                    }
                    else
                    {
                        MessageBox.Show("Nie masz dość żelaza!");
                    }
                }
                if (whatToDo == 6)
                {
                    if (coal > howMuch)
                    {
                        gold += howMuchInGold;
                        coal -= howMuch;
                    }
                    else
                    {
                        MessageBox.Show("Nie masz dość węgla!");
                    }
                }
                if (whatToDo == 7)
                {
                    if (foodGrain > howMuch)
                    {
                        gold += howMuchInGold;
                        foodGrain -= howMuch;
                    }
                    else
                    {
                        MessageBox.Show("Nie masz dość złota!");
                    }
                }
                if (whatToDo == 8)
                {
                    if (foodMeat > howMuch)
                    {
                        gold += howMuchInGold;
                        foodMeat -= howMuch;
                    }
                    else
                    {
                        MessageBox.Show("Nie masz dość mięsa!");
                    }

                }
            }
            catch (FormatException)
            {
                MessageBox.Show("Najpier coś wpisz!");
            }
            catch (ArgumentNullException)
            {
                MessageBox.Show("Najpier coś wpisz!");
            }
            updateGUI();// Aktualizuje ilość surowcó
        }

        /// <summary>
        /// Zamykanie okna na przycisku wróć
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close(); // Zamykanie okna
        }


        /// <summary>
        /// funkcja, która wukonuje się, gdy zamykamy okno, tutaj właśnie używam setterów na obiekcie
        /// klasy MainForm, aby zmienić ilość surowców
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void formClosing(object sender, EventArgs e)
        {
            parentReference.setGold(gold);
            parentReference.setCoal(coal);
            parentReference.setIron(iron);
            parentReference.setFoodMeat(foodMeat);
            parentReference.setFoodGrain(foodGrain);
        }


        /// <summary>
        /// Przycisk kupna żelaza
        /// ustawia w labelu odpowiedni napis,
        /// oraz zmienną whatToDo na 1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonIronBuy_Click(object sender, EventArgs e)
        {
            labelHowMuchSellOrBuy.Text = "Ile żelaza chcesz kupić?";
            labelHowMuchSellOrBuy.Visible = true;
            whatToDo = 1;
            this.textBoxHowMuchSellOrBuy.Text = ""; // zeruje textbox, aby było łatwiej wprowadzić nową wartość
            this.textBoxPriceInGold.Text = ""; // zeruje textbox, aby nie zostało w nim poprzedniej wartości
        }
        /// <summary>
        /// Przycisk kupna węgla
        /// ustawia w labelu odpowiedni napis,
        /// oraz zmienną whatToDo na 2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCoalBuy_Click(object sender, EventArgs e)
        {
            labelHowMuchSellOrBuy.Text = "Ile węgla chcesz kupić?";
            labelHowMuchSellOrBuy.Visible = true;
            whatToDo = 2;
            this.textBoxHowMuchSellOrBuy.Text = ""; // zeruje textbox, aby było łatwiej wprowadzić nową wartość
            this.textBoxPriceInGold.Text = ""; // zeruje textbox, aby nie zostało w nim poprzedniej wartości
        }
        /// <summary>
        /// Przycisk kupna zboża
        /// ustawia w labelu odpowiedni napis,
        /// oraz zmienną whatToDo na 3
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFoodGrainBuy_Click(object sender, EventArgs e)
        {
            labelHowMuchSellOrBuy.Text = "Ile zboża chcesz kupić?";
            labelHowMuchSellOrBuy.Visible = true;
            whatToDo = 3;
            this.textBoxHowMuchSellOrBuy.Text = ""; // zeruje textbox, aby było łatwiej wprowadzić nową wartość
            this.textBoxPriceInGold.Text = ""; // zeruje textbox, aby nie zostało w nim poprzedniej wartości
        }
        /// <summary>
        /// Przycisk kupna mięsa
        /// ustawia w labelu odpowiedni napis,
        /// oraz zmienną whatToDo na 4
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFoodMeatBuy_Click(object sender, EventArgs e)
        {
            labelHowMuchSellOrBuy.Text = "Ile mięsa chcesz kupić?";
            labelHowMuchSellOrBuy.Visible = true;
            whatToDo = 4;
            this.textBoxHowMuchSellOrBuy.Text = ""; // zeruje textbox, aby było łatwiej wprowadzić nową wartość
            this.textBoxPriceInGold.Text = ""; // zeruje textbox, aby nie zostało w nim poprzedniej wartości
        }
        /// <summary>
        /// Przycisk sprzedarzy żelaza
        /// ustawia w labelu odpowiedni napis,
        /// oraz zmienną whatToDo na 5
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonIronSell_Click(object sender, EventArgs e)
        {
            labelHowMuchSellOrBuy.Text = "Ile żelaza chcesz sprzedać?";
            labelHowMuchSellOrBuy.Visible = true;
            whatToDo = 5;
            this.textBoxHowMuchSellOrBuy.Text = ""; // zeruje textbox, aby było łatwiej wprowadzić nową wartość
            this.textBoxPriceInGold.Text = ""; // zeruje textbox, aby nie zostało w nim poprzedniej wartości
        }
        /// <summary>
        /// Przycisk sprzedarzy węgla
        /// ustawia w labelu odpowiedni napis,
        /// oraz zmienną whatToDo na 6
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCoalSell_Click(object sender, EventArgs e)
        {
            labelHowMuchSellOrBuy.Text = "Ile węgla chcesz sprzedać?";
            labelHowMuchSellOrBuy.Visible = true;
            whatToDo = 6;
            this.textBoxHowMuchSellOrBuy.Text = ""; // zeruje textbox, aby było łatwiej wprowadzić nową wartość
            this.textBoxPriceInGold.Text = ""; // zeruje textbox, aby nie zostało w nim poprzedniej wartości
        }
        /// <summary>
        /// Przycisk sprzedarzy zboża
        /// ustawia w labelu odpowiedni napis,
        /// oraz zmienną whatToDo na 7
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFoodGrainSell_Click(object sender, EventArgs e)
        {
            labelHowMuchSellOrBuy.Text = "Ile zboża chcesz sprzedać?";
            labelHowMuchSellOrBuy.Visible = true;
            whatToDo = 7;
            this.textBoxHowMuchSellOrBuy.Text = ""; // zeruje textbox, aby było łatwiej wprowadzić nową wartość
            this.textBoxPriceInGold.Text = ""; // zeruje textbox, aby nie zostało w nim poprzedniej wartości
        }
        /// <summary>
        /// Przycisk sprzedarzy mięsa
        /// ustawia w labelu odpowiedni napis,
        /// oraz zmienną whatToDo na 8
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFoodMeatSell_Click(object sender, EventArgs e)
        {
            labelHowMuchSellOrBuy.Text = "Ile mięsa chcesz sprzedać?";
            labelHowMuchSellOrBuy.Visible = true;
            whatToDo = 8;
            this.textBoxHowMuchSellOrBuy.Text = ""; // zeruje textbox, aby było łatwiej wprowadzić nową wartość
            this.textBoxPriceInGold.Text = ""; // zeruje textbox, aby nie zostało w nim poprzedniej wartości
        }

        /// <summary>
        /// Funkcja wykonywująca się za każdym razem, gdy zmienia się tekst w textboxie textBoxHowMuchSellOrBuy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxHowMuchSellOrBuy_TextChanged(object sender, EventArgs e)
        {
            if(textBoxHowMuchSellOrBuy.Text != "")
            {
                try
                {// konstrukcja try/catch umożliwia "przyłapanie" użytkownika na wpisywaniu liter
                 //do textboxa, w którym planowaliśmy wpisywać liczby
                    string howMuchString = this.textBoxHowMuchSellOrBuy.Text; // ilość towaru w stringu
                    int howMuch = Int32.Parse(howMuchString); // ilość towaru już w incie po sparsowaniu
                    double howMuchInGold; // ilość złota obliczam poniżej
                    if (whatToDo == 1) // dla każdej możliwości kupna/sprzedarzy obliczam wartość w złocie
                    {                  // i umieszczam w textboxie "textBoxPriceInGold"
                        howMuchInGold = howMuch * ironPriceBuy;
                        this.textBoxPriceInGold.Text = ((int)howMuchInGold).ToString();
                    }
                    if (whatToDo == 2)
                    {
                        howMuchInGold = howMuch * coalPriceBuy;
                        this.textBoxPriceInGold.Text = ((int)howMuchInGold).ToString();
                    }
                    if (whatToDo == 3)
                    {
                        howMuchInGold = howMuch * foodGrainPriceBuy;
                        this.textBoxPriceInGold.Text = ((int)howMuchInGold).ToString();
                    }
                    if (whatToDo == 4)
                    {
                        howMuchInGold = howMuch * foodMeatPriceBuy;
                        this.textBoxPriceInGold.Text = ((int)howMuchInGold).ToString();
                    }
                    if (whatToDo == 5)
                    {
                        howMuchInGold = howMuch * ironPriceSell;
                        this.textBoxPriceInGold.Text = ((int)howMuchInGold).ToString();
                    }
                    if (whatToDo == 6)
                    {
                        howMuchInGold = howMuch * coalPriceSell;
                        this.textBoxPriceInGold.Text = ((int)howMuchInGold).ToString();
                    }
                    if (whatToDo == 7)
                    {
                        howMuchInGold = howMuch * foodGrainPriceSell;
                        this.textBoxPriceInGold.Text = ((int)howMuchInGold).ToString();
                    }
                    if (whatToDo == 8)
                    {
                        howMuchInGold = howMuch * foodMeatPriceSell;
                        this.textBoxPriceInGold.Text = ((int)howMuchInGold).ToString();
                    }
                }
                catch (FormatException)// łapanie odpowiednich wyjątków i komentarz dla użytkownika w messageboxie
                {
                    MessageBox.Show("Wpisz liczbę, a nie litery lub znaki!");
                    this.textBoxHowMuchSellOrBuy.Text = ""; // jeżeli był wyjątek, znaczy że było źle wpisane, więc resetuje textbox
                }
                catch (OverflowException)// łapanie odpowiednich wyjątków i komentarz dla użytkownika w messageboxie
                {
                    MessageBox.Show("Za buża liczba!");
                    this.textBoxHowMuchSellOrBuy.Text = ""; // jeżeli był wyjątek, znaczy że było źle wpisane, więc resetuje textbox
                }
            }
        }
    }
}
