﻿namespace MateuszJanotaZadanieDomoweLab1
{
    partial class TradeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelFoodMeat = new System.Windows.Forms.Label();
            this.labelIron = new System.Windows.Forms.Label();
            this.labelFoodGrain = new System.Windows.Forms.Label();
            this.labelCoal = new System.Windows.Forms.Label();
            this.labelGold = new System.Windows.Forms.Label();
            this.labelFoodMeatAmount = new System.Windows.Forms.Label();
            this.labelFoodGrainAmount = new System.Windows.Forms.Label();
            this.labelCoalAmount = new System.Windows.Forms.Label();
            this.labelGoldAmount = new System.Windows.Forms.Label();
            this.labelIronAmount = new System.Windows.Forms.Label();
            this.buttonIronSell = new System.Windows.Forms.Button();
            this.buttonIronBuy = new System.Windows.Forms.Button();
            this.buttonCoalBuy = new System.Windows.Forms.Button();
            this.buttonCoalSell = new System.Windows.Forms.Button();
            this.buttonFoodGrainBuy = new System.Windows.Forms.Button();
            this.buttonFoodGrainSell = new System.Windows.Forms.Button();
            this.buttonFoodMeatBuy = new System.Windows.Forms.Button();
            this.buttonFoodMeatSell = new System.Windows.Forms.Button();
            this.labelHowMuchSellOrBuy = new System.Windows.Forms.Label();
            this.labelPriceInGold = new System.Windows.Forms.Label();
            this.buttonAgree = new System.Windows.Forms.Button();
            this.textBoxHowMuchSellOrBuy = new System.Windows.Forms.TextBox();
            this.textBoxPriceInGold = new System.Windows.Forms.TextBox();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.labelBetterPrices = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelFoodMeat
            // 
            this.labelFoodMeat.AutoSize = true;
            this.labelFoodMeat.Location = new System.Drawing.Point(375, 9);
            this.labelFoodMeat.Name = "labelFoodMeat";
            this.labelFoodMeat.Size = new System.Drawing.Size(35, 13);
            this.labelFoodMeat.TabIndex = 11;
            this.labelFoodMeat.Text = "Mięso";
            // 
            // labelIron
            // 
            this.labelIron.AutoSize = true;
            this.labelIron.Location = new System.Drawing.Point(95, 9);
            this.labelIron.Name = "labelIron";
            this.labelIron.Size = new System.Drawing.Size(39, 13);
            this.labelIron.TabIndex = 10;
            this.labelIron.Text = "Żelazo";
            // 
            // labelFoodGrain
            // 
            this.labelFoodGrain.AutoSize = true;
            this.labelFoodGrain.Location = new System.Drawing.Point(285, 9);
            this.labelFoodGrain.Name = "labelFoodGrain";
            this.labelFoodGrain.Size = new System.Drawing.Size(37, 13);
            this.labelFoodGrain.TabIndex = 9;
            this.labelFoodGrain.Text = "Zboże";
            // 
            // labelCoal
            // 
            this.labelCoal.AutoSize = true;
            this.labelCoal.Location = new System.Drawing.Point(186, 9);
            this.labelCoal.Name = "labelCoal";
            this.labelCoal.Size = new System.Drawing.Size(40, 13);
            this.labelCoal.TabIndex = 8;
            this.labelCoal.Text = "Węgiel";
            // 
            // labelGold
            // 
            this.labelGold.AutoSize = true;
            this.labelGold.Location = new System.Drawing.Point(10, 9);
            this.labelGold.Name = "labelGold";
            this.labelGold.Size = new System.Drawing.Size(33, 13);
            this.labelGold.TabIndex = 7;
            this.labelGold.Text = "Złoto";
            // 
            // labelFoodMeatAmount
            // 
            this.labelFoodMeatAmount.AutoSize = true;
            this.labelFoodMeatAmount.Location = new System.Drawing.Point(377, 30);
            this.labelFoodMeatAmount.Name = "labelFoodMeatAmount";
            this.labelFoodMeatAmount.Size = new System.Drawing.Size(35, 13);
            this.labelFoodMeatAmount.TabIndex = 18;
            this.labelFoodMeatAmount.Text = "label7";
            // 
            // labelFoodGrainAmount
            // 
            this.labelFoodGrainAmount.AutoSize = true;
            this.labelFoodGrainAmount.Location = new System.Drawing.Point(285, 30);
            this.labelFoodGrainAmount.Name = "labelFoodGrainAmount";
            this.labelFoodGrainAmount.Size = new System.Drawing.Size(35, 13);
            this.labelFoodGrainAmount.TabIndex = 17;
            this.labelFoodGrainAmount.Text = "label6";
            // 
            // labelCoalAmount
            // 
            this.labelCoalAmount.AutoSize = true;
            this.labelCoalAmount.Location = new System.Drawing.Point(186, 30);
            this.labelCoalAmount.Name = "labelCoalAmount";
            this.labelCoalAmount.Size = new System.Drawing.Size(35, 13);
            this.labelCoalAmount.TabIndex = 16;
            this.labelCoalAmount.Text = "label5";
            // 
            // labelGoldAmount
            // 
            this.labelGoldAmount.AutoSize = true;
            this.labelGoldAmount.Location = new System.Drawing.Point(12, 30);
            this.labelGoldAmount.Name = "labelGoldAmount";
            this.labelGoldAmount.Size = new System.Drawing.Size(35, 13);
            this.labelGoldAmount.TabIndex = 15;
            this.labelGoldAmount.Text = "label4";
            // 
            // labelIronAmount
            // 
            this.labelIronAmount.AutoSize = true;
            this.labelIronAmount.Location = new System.Drawing.Point(95, 30);
            this.labelIronAmount.Name = "labelIronAmount";
            this.labelIronAmount.Size = new System.Drawing.Size(35, 13);
            this.labelIronAmount.TabIndex = 14;
            this.labelIronAmount.Text = "label3";
            // 
            // buttonIronSell
            // 
            this.buttonIronSell.Location = new System.Drawing.Point(87, 95);
            this.buttonIronSell.Name = "buttonIronSell";
            this.buttonIronSell.Size = new System.Drawing.Size(57, 23);
            this.buttonIronSell.TabIndex = 19;
            this.buttonIronSell.Text = "Sprzedaj";
            this.buttonIronSell.UseVisualStyleBackColor = true;
            this.buttonIronSell.Click += new System.EventHandler(this.buttonIronSell_Click);
            // 
            // buttonIronBuy
            // 
            this.buttonIronBuy.Location = new System.Drawing.Point(87, 66);
            this.buttonIronBuy.Name = "buttonIronBuy";
            this.buttonIronBuy.Size = new System.Drawing.Size(57, 23);
            this.buttonIronBuy.TabIndex = 20;
            this.buttonIronBuy.Text = "Kup";
            this.buttonIronBuy.UseVisualStyleBackColor = true;
            this.buttonIronBuy.Click += new System.EventHandler(this.buttonIronBuy_Click);
            // 
            // buttonCoalBuy
            // 
            this.buttonCoalBuy.Location = new System.Drawing.Point(178, 66);
            this.buttonCoalBuy.Name = "buttonCoalBuy";
            this.buttonCoalBuy.Size = new System.Drawing.Size(57, 23);
            this.buttonCoalBuy.TabIndex = 22;
            this.buttonCoalBuy.Text = "Kup";
            this.buttonCoalBuy.UseVisualStyleBackColor = true;
            this.buttonCoalBuy.Click += new System.EventHandler(this.buttonCoalBuy_Click);
            // 
            // buttonCoalSell
            // 
            this.buttonCoalSell.Location = new System.Drawing.Point(178, 95);
            this.buttonCoalSell.Name = "buttonCoalSell";
            this.buttonCoalSell.Size = new System.Drawing.Size(57, 23);
            this.buttonCoalSell.TabIndex = 21;
            this.buttonCoalSell.Text = "Sprzedaj";
            this.buttonCoalSell.UseVisualStyleBackColor = true;
            this.buttonCoalSell.Click += new System.EventHandler(this.buttonCoalSell_Click);
            // 
            // buttonFoodGrainBuy
            // 
            this.buttonFoodGrainBuy.Location = new System.Drawing.Point(274, 66);
            this.buttonFoodGrainBuy.Name = "buttonFoodGrainBuy";
            this.buttonFoodGrainBuy.Size = new System.Drawing.Size(57, 23);
            this.buttonFoodGrainBuy.TabIndex = 24;
            this.buttonFoodGrainBuy.Text = "Kup";
            this.buttonFoodGrainBuy.UseVisualStyleBackColor = true;
            this.buttonFoodGrainBuy.Click += new System.EventHandler(this.buttonFoodGrainBuy_Click);
            // 
            // buttonFoodGrainSell
            // 
            this.buttonFoodGrainSell.Location = new System.Drawing.Point(274, 95);
            this.buttonFoodGrainSell.Name = "buttonFoodGrainSell";
            this.buttonFoodGrainSell.Size = new System.Drawing.Size(57, 23);
            this.buttonFoodGrainSell.TabIndex = 23;
            this.buttonFoodGrainSell.Text = "Sprzedaj";
            this.buttonFoodGrainSell.UseVisualStyleBackColor = true;
            this.buttonFoodGrainSell.Click += new System.EventHandler(this.buttonFoodGrainSell_Click);
            // 
            // buttonFoodMeatBuy
            // 
            this.buttonFoodMeatBuy.Location = new System.Drawing.Point(365, 66);
            this.buttonFoodMeatBuy.Name = "buttonFoodMeatBuy";
            this.buttonFoodMeatBuy.Size = new System.Drawing.Size(57, 23);
            this.buttonFoodMeatBuy.TabIndex = 26;
            this.buttonFoodMeatBuy.Text = "Kup";
            this.buttonFoodMeatBuy.UseVisualStyleBackColor = true;
            this.buttonFoodMeatBuy.Click += new System.EventHandler(this.buttonFoodMeatBuy_Click);
            // 
            // buttonFoodMeatSell
            // 
            this.buttonFoodMeatSell.Location = new System.Drawing.Point(365, 95);
            this.buttonFoodMeatSell.Name = "buttonFoodMeatSell";
            this.buttonFoodMeatSell.Size = new System.Drawing.Size(57, 23);
            this.buttonFoodMeatSell.TabIndex = 25;
            this.buttonFoodMeatSell.Text = "Sprzedaj";
            this.buttonFoodMeatSell.UseVisualStyleBackColor = true;
            this.buttonFoodMeatSell.Click += new System.EventHandler(this.buttonFoodMeatSell_Click);
            // 
            // labelHowMuchSellOrBuy
            // 
            this.labelHowMuchSellOrBuy.AutoSize = true;
            this.labelHowMuchSellOrBuy.Location = new System.Drawing.Point(12, 142);
            this.labelHowMuchSellOrBuy.Name = "labelHowMuchSellOrBuy";
            this.labelHowMuchSellOrBuy.Size = new System.Drawing.Size(124, 13);
            this.labelHowMuchSellOrBuy.TabIndex = 27;
            this.labelHowMuchSellOrBuy.Text = "Ile surowca chesz kupić:";
            this.labelHowMuchSellOrBuy.Visible = false;
            // 
            // labelPriceInGold
            // 
            this.labelPriceInGold.AutoSize = true;
            this.labelPriceInGold.Location = new System.Drawing.Point(66, 167);
            this.labelPriceInGold.Name = "labelPriceInGold";
            this.labelPriceInGold.Size = new System.Drawing.Size(78, 13);
            this.labelPriceInGold.TabIndex = 28;
            this.labelPriceInGold.Text = "Cena w złocie:";
            // 
            // buttonAgree
            // 
            this.buttonAgree.Location = new System.Drawing.Point(290, 138);
            this.buttonAgree.Name = "buttonAgree";
            this.buttonAgree.Size = new System.Drawing.Size(122, 23);
            this.buttonAgree.TabIndex = 29;
            this.buttonAgree.Text = "Zatwierdź transakcje";
            this.buttonAgree.UseVisualStyleBackColor = true;
            this.buttonAgree.Click += new System.EventHandler(this.buttonAgree_Click);
            // 
            // textBoxHowMuchSellOrBuy
            // 
            this.textBoxHowMuchSellOrBuy.Location = new System.Drawing.Point(150, 139);
            this.textBoxHowMuchSellOrBuy.Name = "textBoxHowMuchSellOrBuy";
            this.textBoxHowMuchSellOrBuy.Size = new System.Drawing.Size(100, 20);
            this.textBoxHowMuchSellOrBuy.TabIndex = 30;
            this.textBoxHowMuchSellOrBuy.TextChanged += new System.EventHandler(this.textBoxHowMuchSellOrBuy_TextChanged);
            // 
            // textBoxPriceInGold
            // 
            this.textBoxPriceInGold.Enabled = false;
            this.textBoxPriceInGold.Location = new System.Drawing.Point(150, 164);
            this.textBoxPriceInGold.Name = "textBoxPriceInGold";
            this.textBoxPriceInGold.ReadOnly = true;
            this.textBoxPriceInGold.Size = new System.Drawing.Size(100, 20);
            this.textBoxPriceInGold.TabIndex = 31;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(290, 162);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(122, 23);
            this.buttonCancel.TabIndex = 32;
            this.buttonCancel.Text = "Wróć";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // labelBetterPrices
            // 
            this.labelBetterPrices.AutoSize = true;
            this.labelBetterPrices.Location = new System.Drawing.Point(30, 195);
            this.labelBetterPrices.Name = "labelBetterPrices";
            this.labelBetterPrices.Size = new System.Drawing.Size(376, 13);
            this.labelBetterPrices.TabIndex = 33;
            this.labelBetterPrices.Text = "Twój wójt, który wspaniale zna handel i ekonomię, wynegocjował super ceny!!";
            this.labelBetterPrices.Visible = false;
            // 
            // TradeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MateuszJanotaZadanieDomoweLab1.Properties.Resources.shopBackground;
            this.ClientSize = new System.Drawing.Size(436, 215);
            this.Controls.Add(this.labelBetterPrices);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.textBoxPriceInGold);
            this.Controls.Add(this.textBoxHowMuchSellOrBuy);
            this.Controls.Add(this.buttonAgree);
            this.Controls.Add(this.labelPriceInGold);
            this.Controls.Add(this.labelHowMuchSellOrBuy);
            this.Controls.Add(this.buttonFoodMeatBuy);
            this.Controls.Add(this.buttonFoodMeatSell);
            this.Controls.Add(this.buttonFoodGrainBuy);
            this.Controls.Add(this.buttonFoodGrainSell);
            this.Controls.Add(this.buttonCoalBuy);
            this.Controls.Add(this.buttonCoalSell);
            this.Controls.Add(this.buttonIronBuy);
            this.Controls.Add(this.buttonIronSell);
            this.Controls.Add(this.labelFoodMeatAmount);
            this.Controls.Add(this.labelFoodGrainAmount);
            this.Controls.Add(this.labelCoalAmount);
            this.Controls.Add(this.labelGoldAmount);
            this.Controls.Add(this.labelIronAmount);
            this.Controls.Add(this.labelFoodMeat);
            this.Controls.Add(this.labelIron);
            this.Controls.Add(this.labelFoodGrain);
            this.Controls.Add(this.labelCoal);
            this.Controls.Add(this.labelGold);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.formClosing);
            // Powyższa linijka to dodanie nasłuchu wydarzenia zamknięcia, abym mógł wtedy użyć setterów
            // klasy MainForm
            this.Name = "TradeForm";
            this.Text = "TradeForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelFoodMeat;
        private System.Windows.Forms.Label labelIron;
        private System.Windows.Forms.Label labelFoodGrain;
        private System.Windows.Forms.Label labelCoal;
        private System.Windows.Forms.Label labelGold;
        private System.Windows.Forms.Label labelFoodMeatAmount;
        private System.Windows.Forms.Label labelFoodGrainAmount;
        private System.Windows.Forms.Label labelCoalAmount;
        private System.Windows.Forms.Label labelGoldAmount;
        private System.Windows.Forms.Label labelIronAmount;
        private System.Windows.Forms.Button buttonIronSell;
        private System.Windows.Forms.Button buttonIronBuy;
        private System.Windows.Forms.Button buttonCoalBuy;
        private System.Windows.Forms.Button buttonCoalSell;
        private System.Windows.Forms.Button buttonFoodGrainBuy;
        private System.Windows.Forms.Button buttonFoodGrainSell;
        private System.Windows.Forms.Button buttonFoodMeatBuy;
        private System.Windows.Forms.Button buttonFoodMeatSell;
        private System.Windows.Forms.Label labelHowMuchSellOrBuy;
        private System.Windows.Forms.Label labelPriceInGold;
        private System.Windows.Forms.Button buttonAgree;
        private System.Windows.Forms.TextBox textBoxHowMuchSellOrBuy;
        private System.Windows.Forms.TextBox textBoxPriceInGold;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label labelBetterPrices;
    }
}